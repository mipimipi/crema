# Changelog

## [Release 3.1.1](https://gitlab.com/mipimipi/crema/tags/v3.1.1) (2022-08-27)

### Changed

* replaced package gitlab.com/mipimipi/go-utils by packages gitlab.com/go-utilities/*

## [Release 3.1.0](https://gitlab.com/mipimipi/crema/tags/v3.1.0) (2022-08-17)

### Added

* flag to automatically remove chroot container after build

## [Release 3.0.0](https://gitlab.com/mipimipi/crema/tags/v3.0.0) (2022-06-04)

### Added

* command `lsrepos`: list all configured repositories
* script `crema-all`: execute crema commands for all configured repositories

### Changed

* command line interface of crema: comprehensive redesign, consult the manpage for the changed interface

## [Release 2.11.0](https://gitlab.com/mipimipi/crema/tags/v2.11.0) (2022-05-26)

### Added

* new attribute 'DBName' in configuration file: Possibility to set the name of the repository DB explicitly

## [Release 2.10.0](https://gitlab.com/mipimipi/crema/tags/v2.10.0) (2022-05-20)

### Added

* possibility to manipulate chroot container via script directly after creation

## [Release 2.9.0](https://gitlab.com/mipimipi/crema/tags/v2.9.0) (2021-12-27)

### Added

* Flag --ignore-arch

## [Release 2.8.0](https://gitlab.com/mipimipi/crema/tags/v2.8.0) (2021-12-24)

### Added

* extract dependency information from repo DB and use is in ls and rm command
* sign command

### Changed

* extended cleanup logic: signatures are verified, obsolete files are deleted

## [Release 2.7.5](https://gitlab.com/mipimipi/crema/tags/v2.7.5) (2021-12-12)

### Changed

* corrected bug: packages signature file were not moved from temp folder to local repo folder

## [Release 2.7.3](https://gitlab.com/mipimipi/crema/tags/v2.7.3) (2021-11-28)

### Changed

* made determination of system architecture more robust (API of package syscall depends on system architecture)

## [Release 2.7.2](https://gitlab.com/mipimipi/crema/tags/v2.7.2) (2021-11-21)

### Changed

* crema returns with non-zero in case of errors

## [Release 2.7.1](https://gitlab.com/mipimipi/crema/tags/v2.7.1) (2021-11-20)

### Changed

* corrected display problems for the output of stderr of commands that crema calls

## [Release 2.7.0](https://gitlab.com/mipimipi/crema/tags/v2.7.0) (2021-11-20)

### Changed

* determination of pacman.conf and makepkg.conf files for chroot containers
* improved error handling

### Removed

* options for `mkchroot` command for distcc threads and distcc volunteers

## [Release 2.6.1](https://gitlab.com/mipimipi/crema/tags/v2.6.1) (2021-11-13)

### Added

* command `mkchroot` to enable the explicit creation of chroots
* documentation about how to use distributed builds in chroots

### Changed

* naming template for repo-specific pacman.conf files (existing files will be renamed automatically)
* use makepkg-x86_64.conf from devtools as makepkg.conf for chroots 

## [Release 2.5.1](https://gitlab.com/mipimipi/crema/tags/v2.5.1) (2021-11-01)

### Changed

* Simplified creation of new releases/tags/versions
* changed CI/CD config: trigger deployment to AUR automatically when new tag is created

### Removed

* removed CHANGELOG.md. Changes will be listed in GitLab tag release notes, instead 

## [Release 2.5.0](https://gitlab.com/mipimipi/crema/tags/v2.5.0) (2021-10-31)

### Added

* adjusted and extented documentation

### Changed

* always use `/etc/makepkg.conf`
* use `/etc/pacman.conf` as template for repository-specific config files
* resolved license inconsistencies

## [Release 2.4.0](https://gitlab.com/mipimipi/crema/tags/2.4.0) (2021-10-30)

### Changed

* AUR package *crema* will no longer be deployed
* removed `sudo` from creation and update of chroot directory and from call of `makechrootpkg`
* fixed several bugs that occured when *crema* is used on ARM platforms
* updated dependencies

## [Release 2.3.0](https://gitlab.com/mipimipi/crema/tags/2.3.0) (2021-01-28)

### Added

* Completions for bash, zsh and fish.

### Changed

* Reorganized source code 

## [Release 2.2.2](https://gitlab.com/mipimipi/crema/tags/2.2.2) (2021-01-26)

### Added

* Introduced $repo is placeholder for the repository name to crema configuration file

### Changed

* Consider package base when cloning a package repo from AUR (so far the package name was used which is wrong if a PKGBUILD files contains build instruction for more than one package)

## [Release 2.2.1](https://gitlab.com/mipimipi/crema/tags/2.2.1) (2020-04-19)

### Added

* Make the chroot directory mount point by explicitly executing `mount --bind <dir> <dir>` since otherwise `arch-chroot` would complain.

### Changed

* Changed config file format to [TOML](https://en.wikipedia.org/wiki/TOML), which does not change the syntax significantly - remote file paths need to be enclosed in quotation marks.

## [Release 2.2.0](https://gitlab.com/mipimipi/crema/tags/2.2.0) (2020-04-16)

### Added

* Configuration has the new option `SignDB`. It can either be `true` or `false` and specifies if the repository is signed.
* Like in `pacman.conf`, `$arch` can be used as placeholder for the architecture in the configuration file.
* Command `crema update` asks the user for confirmation before applying AUR updates. This behaviour can be switched off with the flag `--noconfirm`.

### Changed

* Complete re-implementation in [Golang](https://golang.org/). This was necessary since over time, crema grew to a monster of far more than 1,000 lines of bash code, which is quite challenging to manage.
* `crema update` is now only possible for one repository.
* Renamed flag `--no-chroot` of the commands `crema add` and `crema update` to `--nochroot`.
* Standard location for crema data changed to `~/.cache/crema` (before,  that was `~/.local/crema`).

### Removed

* Dependency to [aurutils](https://github.com/AladW/aurutils) / a crema-specific fork of aurutils. Now, crema invokes tools like [makechrootpkg](https://wiki.archlinux.org/index.php/DeveloperWiki:Building_in_a_clean_chroot#Building_in_the_chroot), [makepkg](https://wiki.archlinux.org/index.php/Makepkg), [repo-add](https://www.archlinux.org/pacman/repo-add.8.html) and repo-remove directly.
* Command `crema version`. The version can be displayed via `crema --version`.
* Flag `--sign` of command `crema update`. Now, packages will be signed if they were also signed in the past.

## [Release 2.1.2](https://gitlab.com/mipimipi/crema/tags/2.1.2) (2020-02-15)

### Added

* New option `--no-chroot` to override the default and to not build packages in a chroot container. This possibility is particularly useful if crema is run in a container as part of a CI/CD pipeline where it's impossible or difficult to run privileged.

## [Release 2.1](https://gitlab.com/mipimipi/crema/tags/2.1) (2020-01-18)

### Added

* Lock mechanism added to avoid parallel write access to the local data of a repository whcih could lead to an inconsistent repository
* Command `clear` extended by options `--cache` and `--chroot` do delete the local copy and/or the chroot container of a repository

### Changed

* Format of configuration file changed to [INI](https://en.wikipedia.org/wiki/INI_file)
* Local copy of remote repositories are now kept after a run of crema, which improves the performance.

### Removed

* Commands `repo-add` and `repo-rm` removed. These commands are obsolete since the configuration file is now in standard INI format, and thus it can be expected to be changed manually via text editor.
* Command `chroot-rm` removed. The functionality of this command is now covered by the extended `clear` command.
* Command `env` removed.

## [Release 2.0](https://gitlab.com/mipimipi/crema/tags/2.0) (2020-01-02)

### Added

* crema man page that contains comprehensive documentation
* Each repository that is managed by crema has a dedicated chroot container where packages are built and a dedicated `pacman.conf`file that is used for the build process - see the crema man page for more detailed information. 
* System-wide crema configuration. It's stored under `/etc/crema` - see the crema man page for more detailed information.
* `crema ls` displays the sigining status of packages and repository databases (the information whether they are signed or not)
* `crema env` displays information about the repository-specific chroot container and `pacman.conf`file, and also information about the gpg key used for siging packages (provided it is set).
* Slightly modified fork of [aurutils 2.3.3](https://github.com/aladw/aurutils)

### Changed

* The user-specific crema configuration has been moved from `~/.config/crema.conf` to `~/.config/crema/repos.conf`.
* crema builds packages only in chroot containers via `makechrootpkg`.
* For the `crema add` command, the options for specifying packages have been changed. The options `--from:aur` and `--from:local` are no longer available. They have been replaced by new options `--aur` and `--local`. Now, for each package it must be specified whether it comes from AUR or from the local file system. Thus it is possible to add packages from AUR and local packages with a single call of `crema add`.
* `crema chroot-rm` has a new option `--repo`. Thus, chroot containers of a specific repository can be removed 

### Removed

* Command `chroot-add-key`, since it's obsolete - see crema man page for handling of key problems
* Commands `sign`, `unsign`, since they are no longer needed
* Option `--nochroot`, since it's obsolete.


## [Release 1.5](https://gitlab.com/mipimipi/crema/tags/1.5) (2019-07-17)

### Added

* New commands `chroot-add-key`, `chroot-rm` and `purge`.
* Automated cleanup when packages are added or updated

### Changed

* Renamed command `build` to `add`.

## [Release 1.4](https://gitlab.com/mipimipi/crema/tags/1.4) (2019-06-29)

### Added

* New commands `sign` and `unsign` to sign / unsign an entire repository with gpg (incl. repository database and package files)
* Additional checks for `cleanup` command:
    * Check for package files that do not have a corresponding entry in the package database
    * Check for signature files that do not have a corresponding data file
* Convenience logic for signing: If a repository is signed, and it shall be changed without but the `--sign` flag isn't set, the flag is set implicitly
* New commands `repo-add` and `repo-rm` to maintain the crema configuration.

### Changed

* Combined `add` and `build`commands:
    * So far, both commands built packages and added them to a repository. The only difference was the sources: Whereas `add` took PKGBUILD's from AUR, `build` took them from the local filesystem.
    * Now, there's only the `build` command which has an addtional flag `--from:<source>` where `<source>` can either be `aur` or `local`
* Per default, `makechrootpkg` is now used to build packages. If `makepkg` shall be used instead, the flag `--nochroot` has to be set.
* Comprehensive refactoring

### Removed

* Flag `--chroot` since it is obsolete.

## [Release 1.3](https://gitlab.com/mipimipi/crema/tags/1.3) (2019-06-19)

### Added

* Support of `makechrootpkg` to build packages. Default is `makepkg`.
* Commands `ls` and `update` can now work for all repositories at once.
* New command `cleanup` to make a repository consistent.

## [Release 1.2](https://gitlab.com/mipimipi/crema/tags/1.2) (2019-06-08)

### Added

* Management of multiple repositories
* Possibility to build packages from multiple PKGBUILD files.
* Possibility to sign packages

### Changed

* Replaced environment variables `CREMAREPO` and `CREMAREPODIR` by configuration file

## [Release 1.1](https://gitlab.com/mipimipi/crema/tags/1.1) (2019-05-20)

### Added

* `ls` sub command: Possibility to list all packages that are contained in the custom repository.


## [Release 1.0](https://gitlab.com/mipimipi/crema/tags/1.0) (2019-05-12)

First release suited for productive use.
