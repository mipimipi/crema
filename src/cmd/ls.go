package main

import (
	"fmt"

	"github.com/spf13/cobra"
	c "gitlab.com/mipimipi/crema/src/internal"
)

// lsCmd represents the ls command
var lsCmd = &cobra.Command{
	Use:   "ls repository",
	Short: "List packages of a repository",
	Long: `List the packages of a repository with their versions and if they are
signed and if other packages depend on them. It is also indicated whether the
repository DB is signed.`,
	SilenceUsage:          true,
	DisableFlagsInUseLine: true,
	Args: func(cmd *cobra.Command, args []string) error {
		// arg must be a valid repository
		if len(args) < 1 {
			return fmt.Errorf("repository is required")
		}
		exists, err := c.IsRepo(args[0])
		if err != nil {
			return err
		}
		if !exists {
			return fmt.Errorf("repository '%s' is not configured", args[0])
		}
		// besides the repo, no other args are allowed
		if len(args) > 1 {
			return fmt.Errorf("only the repository is required, other arguments do not make sense")
		}
		return nil
	},
	ValidArgsFunction: repoCompletion,
	Run: func(cmd *cobra.Command, args []string) {
		c.ReposByNames([]string{args[0]}).Map(
			func(repo c.Rp) c.Rp {
				return repo.ListPackages()
			})
	},
}

func init() {
	rootCmd.AddCommand(lsCmd)
}
