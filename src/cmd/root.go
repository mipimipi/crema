package main

import (
	"os"
	"sort"

	"github.com/spf13/cobra"
	"gitlab.com/go-utilities/msg"
	c "gitlab.com/mipimipi/crema/src/internal"
)

var preamble = `crema (Custom Repository Management) ` + Version + `
Copyright (C) 2019-2022 Michael Picht <https://gitlab.com/mipimipi/crema>

crema helps to manage remote custom repositories.

Web site: https://gitlab.com/mipimipi/crema/

crema comes with ABSOLUTELY NO WARRANTY. This is free software, and you are
welcome to redistribute it under certain conditions. See the GNU General Public
Licence for details.`

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:     "crema [command] [repository] [flags]",
	Short:   "crema helps to manage remote custom repositories",
	Long:    preamble,
	Version: Version,
}

var (
	all        bool
	noconfirm  bool
	nochroot   bool
	ignoreArch bool
)

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		msg.PrintErrorln("%v", err)
		os.Exit(1)
	}
}

// repoCompletion is the completion function for repositories
func repoCompletion(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
	if len(args) == 0 {
		return repoNames(toComplete), cobra.ShellCompDirectiveNoFileComp
	}
	return []string{}, cobra.ShellCompDirectiveNoFileComp
}

// repoNames retrieves repository names from config file. If name is not empty,
// only repository names are returned that bstart with name. Otherweise, all
// repository names are returned
func repoNames(name string) (repoNames []string) {
	for _, repo := range c.ReposByNames([]string{}) {
		if len(name) == 0 || len(repo.Name) >= len(name) && repo.Name[:len(name)] == name {
			repoNames = append(repoNames, repo.Name)
		}
	}
	sort.Strings(repoNames)
	return
}
