package main

import (
	"fmt"

	"github.com/spf13/cobra"
	c "gitlab.com/mipimipi/crema/src/internal"
)

// cleanupCmd represents the cleanup command
var cleanupCmd = &cobra.Command{
	Use:   "cleanup repository",
	Short: "Clean up a repository",
	Long: `To make sure that the repository DB and the package files are consistent to each
other, it is checked that all package files belong to package (versions) that
are contained in the repository DB.
It is also checked that all signature files fit to their counterpart files.`,
	SilenceUsage:          true,
	DisableFlagsInUseLine: true,
	Args: func(cmd *cobra.Command, args []string) error {
		// arg must be a valid repository
		if len(args) < 1 {
			return fmt.Errorf("repository is required")
		}
		exists, err := c.IsRepo(args[0])
		if err != nil {
			return err
		}
		if !exists {
			return fmt.Errorf("repository '%s' is not configured", args[0])
		}
		// besides the repo, no other args are allowed
		if len(args) > 1 {
			return fmt.Errorf("only the repository is required, other arguments do not make sense")
		}
		return nil
	},
	ValidArgsFunction: repoCompletion,
	Run: func(cmd *cobra.Command, args []string) {
		c.ReposByNames([]string{args[0]}).Map(
			func(repo c.Rp) c.Rp {
				return repo.Cleanup()
			})
	},
}

func init() {
	rootCmd.AddCommand(cleanupCmd)
}
