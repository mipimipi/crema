package main

import (
	"os"

	"github.com/spf13/cobra"
)

// gencompCmd represents the gencomp command
var gencompCmd = &cobra.Command{
	Use:                   "gencomp [bash|zsh|fish|powershell]",
	Short:                 "Generate completion script",
	Long:                  "Generate completion scripts for different shells",
	DisableFlagsInUseLine: true,
	ValidArgs:             []string{"bash", "zsh", "fish"},
	Args:                  cobra.ExactValidArgs(1),
	Hidden:                true,
	Run: func(cmd *cobra.Command, args []string) {
		switch args[0] {
		case "bash":
			_ = cmd.Root().GenBashCompletion(os.Stdout)
		case "zsh":
			_ = cmd.Root().GenZshCompletion(os.Stdout)
		case "fish":
			_ = cmd.Root().GenFishCompletion(os.Stdout, true)
		}
	},
}

func init() {
	rootCmd.AddCommand(gencompCmd)
}
