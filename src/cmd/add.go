package main

import (
	"fmt"

	"github.com/spf13/cobra"
	c "gitlab.com/mipimipi/crema/src/internal"
)

// addCmd represents the add command
var addCmd = &cobra.Command{
	Use:   "add repository [flags] {--aur package | --directory local-directory}+",
	Short: "Build and add packages to a repository",
	Long: `Build and add packages to a repository that can either be from the AUR or from
PKGBUILD files that are stored in the local file system. The packages can be
signed. For this, the environment variable GPGKEY must contain the id of the
corresponding gpg key`,
	SilenceUsage:          true,
	DisableFlagsInUseLine: true,
	Args: func(cmd *cobra.Command, args []string) error {
		// first arg must be a valid repository
		if len(args) < 1 {
			return fmt.Errorf("repository is required")
		}
		exists, err := c.IsRepo(args[0])
		if err != nil {
			return err
		}
		if !exists {
			return fmt.Errorf("repository '%s' is not configured", args[0])
		}
		// besides the repo, no other args are allowed
		if len(args) > 1 {
			return fmt.Errorf("only the repository is required, other arguments (except flags and their arguments) do not make sense")
		}
		return nil
	},
	ValidArgsFunction: repoCompletion,
	RunE: func(cmd *cobra.Command, args []string) error {
		return c.Repo(args[0]).Add(
			clean,
			sign,
			nochroot,
			append(
				c.PKGBUILDsFromAUR(aurPkgs, ignoreArch),
				c.PKGBUILDsFromLocal(localPkgs, ignoreArch)...,
			),
		)
	},
}

// flags
var (
	clean     bool
	sign      bool
	localPkgs []string
	aurPkgs   []string
)

func init() {
	rootCmd.AddCommand(addCmd)

	// flags
	addCmd.Flags().BoolVarP(&clean, "clean", "c", false, "Remove chroot environment after build")
	addCmd.Flags().BoolVarP(&sign, "sign", "s", false, "Sign packages")
	addCmd.Flags().BoolVarP(&nochroot, "nochroot", "n", false, "Don't build packages in chroot environment")
	addCmd.Flags().BoolVarP(&ignoreArch, "ignorearch", "A", false, "Ignore architecture(s) specified in PKGBUILD file(s)")
	addCmd.Flags().StringArrayVarP(&localPkgs, "directory", "d", []string{}, "Local directory with PKGBUILD file")
	addCmd.Flags().StringArrayVarP(&aurPkgs, "aur", "a", []string{}, "Name of AUR package")

	// annotations
	_ = addCmd.MarkFlagDirname("directory")
}
