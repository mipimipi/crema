package main

import (
	"fmt"

	"github.com/spf13/cobra"
	c "gitlab.com/mipimipi/crema/src/internal"
)

// rmCmd represents the rm command
var rmCmd = &cobra.Command{
	Use:   "rm repository packages",
	Short: "Remove packages from a repository",
	Long: `Packages are removed from the repository DB, and all related package files are
deleted. This includes all existing signature files.`,
	SilenceUsage: true,
	Args: func(cmd *cobra.Command, args []string) error {
		// first arg must be a valid repository
		if len(args) < 1 {
			return fmt.Errorf("repository is required")
		}
		exists, err := c.IsRepo(args[0])
		if err != nil {
			return err
		}
		if !exists {
			return fmt.Errorf("repository '%s' is not configured", args[0])
		}
		// at least one package must be set
		if len(args) == 1 {
			return fmt.Errorf("no packages set for removal")
		}
		return nil
	},
	ValidArgsFunction: repoCompletion,
	RunE: func(cmd *cobra.Command, args []string) error {
		return c.Repo(args[0]).Remove(noconfirm, args[1:])
	},
}

func init() {
	rootCmd.AddCommand(rmCmd)

	// flags
	rmCmd.Flags().BoolVarP(&noconfirm, "noconfirm", "", false, "Don't ask for confirmation and remove packages directly")
}
