package main

import (
	"fmt"

	"github.com/spf13/cobra"
	c "gitlab.com/mipimipi/crema/src/internal"
)

// updateCmd represents the update command
var updateCmd = &cobra.Command{
	Use:   "update repository {--all | packages}",
	Short: "Update outdated AUR packages of a repository",
	Long: `Updates either all or only specific AUR packages of a repository. The packages
for which a newer version exist in AUR are determined, new package versions are
built and replace the current versions in the repository.

If the old package version was signed, the new version will also be signed.
Therefore, the environment variable GPGKEY must contain the id of the
corresponding gpg key.`,
	SilenceUsage: true,
	Args: func(cmd *cobra.Command, args []string) error {
		// first arg must be a valid repository
		if len(args) < 1 {
			return fmt.Errorf("repository is required")
		}
		exists, err := c.IsRepo(args[0])
		if err != nil {
			return err
		}
		if !exists {
			return fmt.Errorf("repository '%s' is not configured", args[0])
		}
		// if --all is set, no packages must set
		if all && len(args) > 1 {
			return fmt.Errorf("since '--all' is set, setting packages in addition does not make sense")
		}
		// if --all is not set, at least one package must be set
		if !all && len(args) == 1 {
			return fmt.Errorf("no packages set for updating")
		}
		return nil
	},
	ValidArgsFunction: repoCompletion,
	RunE: func(cmd *cobra.Command, args []string) error {
		return c.Repo(args[0]).Update(nochroot, noconfirm, ignoreArch, args[1:])
	},
}

func init() {
	rootCmd.AddCommand(updateCmd)

	// flags
	updateCmd.Flags().BoolVarP(&all, "all", "", false, "Update all packages")
	updateCmd.Flags().BoolVarP(&nochroot, "nochroot", "n", false, "Don't build packages in chroot environment")
	updateCmd.Flags().BoolVarP(&noconfirm, "noconfirm", "", false, "Don't ask for confirmation and apply the updates directly")
	updateCmd.Flags().BoolVarP(&ignoreArch, "ignorearch", "A", false, "Ignore architecture(s) specified in PKGBUILD file(s)")
}
