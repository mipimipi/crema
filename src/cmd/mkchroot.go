package main

import (
	"fmt"

	"github.com/spf13/cobra"
	c "gitlab.com/mipimipi/crema/src/internal"
)

// updateCmd represents the update command
var mkChrootCmd = &cobra.Command{
	Use:                   "mkchroot repository",
	Short:                 "Create a chroot container for a repository",
	Long:                  `Creates a chroot container to be used for building packages for a repository.`,
	SilenceUsage:          true,
	DisableFlagsInUseLine: true,
	Args: func(cmd *cobra.Command, args []string) error {
		// arg must be a valid repository
		if len(args) < 1 {
			return fmt.Errorf("repository is required")
		}
		exists, err := c.IsRepo(args[0])
		if err != nil {
			return err
		}
		if !exists {
			return fmt.Errorf("repository '%s' is not configured", args[0])
		}
		// besides the repo, no other args are allowed
		if len(args) > 1 {
			return fmt.Errorf("only the repository is required, other arguments do not make sense")
		}
		return nil
	},
	ValidArgsFunction: repoCompletion,
	RunE: func(cmd *cobra.Command, args []string) error {
		return c.Repo(args[0]).MkChroot()
	},
}

func init() {
	rootCmd.AddCommand(mkChrootCmd)
}
