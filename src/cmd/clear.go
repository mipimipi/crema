package main

import (
	"fmt"

	"github.com/spf13/cobra"
	c "gitlab.com/mipimipi/crema/src/internal"
)

// clearCmd represents the clear command
var clearCmd = &cobra.Command{
	Use:          "clear repository",
	Short:        "Delete local data of a repository",
	Long:         "Delete the chroot container and/or the local copy/cache of a repository.",
	SilenceUsage: true,
	Args: func(cmd *cobra.Command, args []string) error {
		// first arg must be a valid repository
		if len(args) < 1 {
			return fmt.Errorf("repository is required")
		}
		exists, err := c.IsRepo(args[0])
		if err != nil {
			return err
		}
		if !exists {
			return fmt.Errorf("repository '%s' is not configured", args[0])
		}
		// besides the repo, no other args are allowed
		if len(args) > 1 {
			return fmt.Errorf("only the repository is required, other arguments (except flags) do not make sense")
		}
		return nil
	},
	ValidArgsFunction: repoCompletion,
	RunE: func(cmd *cobra.Command, args []string) error {
		return c.Repo(args[0]).Clear(chroot, cache)
	},
}

//flags
var (
	chroot bool
	cache  bool
)

func init() {
	rootCmd.AddCommand(clearCmd)

	// flags
	clearCmd.Flags().BoolVarP(&chroot, "chroot", "", false, "Clear chroot environment")
	clearCmd.Flags().BoolVarP(&cache, "cache", "", false, "Clear local repository cache")
}
