package main

import (
	"fmt"

	"github.com/spf13/cobra"
	c "gitlab.com/mipimipi/crema/src/internal"
)

// signCmd represents the sign command
var signCmd = &cobra.Command{
	Use:   "sign repository {--all | packages}",
	Short: "Sign packages of a repository",
	Long: `Signs either all or only specific packages of a repo. The repository DB is
signed as well if that is required by the crema configuration.`,
	SilenceUsage:          true,
	DisableFlagsInUseLine: true,
	Args: func(cmd *cobra.Command, args []string) error {
		// first arg must be a valid repository
		if len(args) < 1 {
			return fmt.Errorf("repository is required")
		}
		exists, err := c.IsRepo(args[0])
		if err != nil {
			return err
		}
		if !exists {
			return fmt.Errorf("repository '%s' is not configured", args[0])
		}
		// if --all is set, no packages must set
		if all && len(args) > 1 {
			return fmt.Errorf("since '--all' is set, setting packages in addition does not make sense")
		}
		// if --all is not set, at least one package must be set
		if !all && len(args) == 1 {
			return fmt.Errorf("no packages set for signing")
		}
		return nil
	},
	ValidArgsFunction: repoCompletion,
	RunE: func(cmd *cobra.Command, args []string) error {
		return c.Repo(args[0]).Sign(args[1:])
	},
}

func init() {
	rootCmd.AddCommand(signCmd)

	// flags
	signCmd.Flags().BoolVarP(&all, "all", "", false, "Sign all packages")
}
