package main

import (
	"github.com/spf13/cobra"
	c "gitlab.com/mipimipi/crema/src/internal"
)

// lsrepos represents the lsrepos command
var lsreposCmd = &cobra.Command{
	Use:                   "lsrepos",
	Short:                 "List all repositories",
	Long:                  "List all repositories thet are defined in the configuration",
	SilenceUsage:          true,
	DisableFlagsInUseLine: true,
	RunE: func(cmd *cobra.Command, args []string) error {
		return c.ListRepos()
	},
}

func init() {
	rootCmd.AddCommand(lsreposCmd)
}
