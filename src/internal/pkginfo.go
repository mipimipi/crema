package crema

import (
	"fmt"
	"path/filepath"
	"regexp"

	"github.com/pkg/errors"
	"gitlab.com/go-utilities/msg"
)

// reg exp for package name strings ("<pkg-name>-<pkg-version>-<rel-no>")
var rePkginfo = regexp.MustCompile(`^(.*)-([^-]+-[^-]+)$`)

// pkginfo contains information about a package
type pkginfo struct {
	name    pkgname
	version pkgversion
	base    pkgbase
}

// pkginfo array
type pkginfos []pkginfo

// implement sort interface for pkginfo array
func (infos pkginfos) Len() int { return len(infos) }
func (infos pkginfos) Swap(i, j int) {
	infos[i].name, infos[j].name = infos[j].name, infos[i].name
	infos[i].version, infos[j].version = infos[j].version, infos[i].version
	infos[i].base, infos[j].base = infos[j].base, infos[i].base
}
func (infos pkginfos) Less(i, j int) bool { return infos[i].name < infos[j].name }

type pkginfoMap map[pkgname]pkginfo

// newInfo creates a pkginfo from a package string. Attribute base is left empty
func newInfo(pkg pkg) pkginfo {
	a := rePkginfo.FindStringSubmatch(string(pkg))
	if len(a) != 3 {
		panic(fmt.Sprintf("package string '%s' has invalid format", pkg))
	}
	return pkginfo{
		name:    pkgname(a[1]),
		version: pkgversion(a[2]),
	}
}

// signed determines if a package file is signed
func (info pkginfo) signed(repo Rp) (bool, error) {
	// check for architecture=any
	pattern := filepath.Join(repo.localDir, string(info.name)+"-"+string(info.version)+"-any.pkg.tar.*.sig")
	matches, err := filepath.Glob(pattern)
	if err != nil {
		return false, errors.Wrapf(err, "cannot find matches for pattern '%s'", pattern)
	}
	if len(matches) > 0 {
		return true, nil
	}

	// check for dedicated architecture
	arch, err := arch()
	if err != nil {
		return false, errors.Wrapf(err, "cannot determine whether package '%s %s' is signed", info.name, info.version)
	}
	pattern = filepath.Join(repo.localDir, string(info.name)+"-"+string(info.version)+"-"+arch+".pkg.tar.*.sig")
	matches, err = filepath.Glob(pattern)
	if err != nil {
		return false, errors.Wrapf(err, "cannot find matches for pattern '%s'", pattern)
	}
	return len(matches) > 0, nil
}

// bases extracts the distinct package bases from the map into a pkgbases array
func (infos pkginfoMap) bases() (bases pkgbases) {
	b := make(map[pkgbase]struct{})

	for _, info := range infos {
		if _, exists := b[info.base]; !exists {
			b[info.base] = struct{}{}
			bases = append(bases, info.base)
		}
	}
	return
}

// names extracts the keys of the map into a names array
func (infos pkginfoMap) names() (names pkgnames) {
	for name := range infos {
		names = append(names, name)
	}
	return
}

// string returns a string representation of pkginfo
func (info pkginfo) string() string {
	return fmt.Sprintf("%s %s", info.name, info.version)
}

// updatesFromAUR retrieves information about newer packages from AUR. A map of
// pkginfos for the packages where updates exists is returned
func (infos pkginfoMap) updatesFromAUR() pkginfoMap {
	// anything to do at all?
	if len(infos) == 0 {
		return nil
	}

	// fetch package info from AUR
	aurInfo, err := infoFromAUR(infos.names())
	if err != nil {
		return nil
	}

	updates := make(pkginfoMap)

	// determine need for update
	for _, res := range aurInfo.Results {
		infoOld, ok := infos[pkgname(res.Name)]
		if ok {
			var base pkgbase
			if len(res.PackageBase) == 0 {
				base = pkgbase(res.Name)
			} else {
				base = pkgbase(res.PackageBase)
			}
			lessThan, err := infoOld.version.lessThan(pkgversion(res.Version))
			if err != nil {
				fmt.Println(err)
				msg.PrintErrorln("cannot compare versions of package '%s'", infoOld.name)
				continue
			}
			if lessThan {
				updates[pkgname(res.Name)] = pkginfo{
					name:    pkgname(res.Name),
					version: pkgversion(res.Version),
					base:    base}
			}
		}
	}

	return updates
}
