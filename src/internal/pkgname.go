package crema

import (
	"fmt"
	"path/filepath"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/go-utilities/msg"
	str "gitlab.com/go-utilities/strings"
)

// pkgname is the name of a package
type pkgname string
type pkgnames []pkgname
type pkgnameMap map[pkgname]struct{}

// deps represents dependencies: mapping from dependency to dependent packages
type deps map[pkgname]pkgnameMap

// nameFromDepStr takes a string of the form "<pkg-name><operand><version>" or
// "<pkg-name>:<description>" and extracts the package name from it
func nameFromDepStr(s string) (pkgname, error) {
	a := str.SplitMulti(s, "<>=:")
	if len(a) == 0 {
		return pkgname(""), fmt.Errorf("'%s' is not a valid dependency string", s)
	}
	return pkgname(strings.TrimSpace(a[0])), nil
}

// strToNames converts an array of strings to an array of pkgname
func strToNames(ss []string) (names pkgnames) {
	for _, s := range ss {
		names = append(names, pkgname(s))
	}
	return
}

// existsInAUR checks if packages given in names exist in AUR. A map is returned
// that contains the pkginfos of the packages that exist in AUR
func (names pkgnames) existsInAUR() (infos pkginfoMap) {
	// fetch package info from AUR
	aurInfo, err := infoFromAUR(names)
	if err != nil {
		return
	}

	// retrieve package name from AUR response
	infos = make(pkginfoMap)
	for _, res := range aurInfo.Results {
		var base pkgbase
		if len(res.PackageBase) == 0 {
			base = pkgbase(res.Name)
		} else {
			base = pkgbase(res.PackageBase)
		}
		infos[pkgname(res.Name)] = pkginfo{
			name:    pkgname(res.Name),
			version: pkgversion(res.Version),
			base:    base,
		}
	}

	// print error message for packages that could not be read from AUR
	for _, name := range names {
		if _, exists := infos[name]; !exists {
			msg.PrintErrorln("package %s not contained in AUR", name)
		}
	}

	return
}

// file creates an instance of pkgfile from a package name. dir
// is taken as directory. It is verified that such a file really exists in dir.
func (name pkgname) file(dir string) (pkgfile, error) {
	var (
		matches []string
		file    pkgfile
		err     error
	)
	pattern := filepath.Join(dir, string(name)+"-*-*-*.pkg.tar.*")
	if matches, err = filepath.Glob(pattern); err != nil {
		return pkgfile(""), errors.Wrapf(err, "cannot determine matching files for pattern '%s'", pattern)
	}
	for _, match := range matches {
		// ignore .sig file
		if filepath.Ext(match) == ".sig" {
			continue
		}
		file = pkgfile(match)
		// since pattern matching just with wildcards as done with the globbing
		// functionality of the filepath package is to coarse grained, an
		// additional check via regexp is required
		if name == file.name() {
			return file, nil
		}
	}
	return pkgfile(""), fmt.Errorf("no package file exists in '%s' for package '%s'", dir, name)
}

// files returns the package files from dir that belong to the packages whose
// names are contained in names.
func (names pkgnames) files(dir string) (pkgfiles, error) {
	files := pkgfiles{}
	for _, name := range names {
		file, err := name.file(dir)
		if err != nil {
			return files, err
		}
		files = append(files, file)
	}
	return files, nil
}

// removeFiles deletes package files of the named package from dir
func (name pkgname) removeFiles(dir string) error {
	// get old package file
	file, err := name.file(dir)
	if err != nil {
		return nil
	}

	// remove files
	return file.remove()
}

// removeFiles deletes package files of the named packages from dir
func (names pkgnames) removeFiles(dir string) {
	for _, name := range names {
		_ = name.removeFiles(dir)
	}
}
