package crema

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/mholt/archiver/v3"
	"github.com/pkg/errors"
	"gitlab.com/go-utilities/exec"
	"gitlab.com/go-utilities/file"
	fp "gitlab.com/go-utilities/filepath"
	"gitlab.com/go-utilities/msg"
	"gitlab.com/go-utilities/reflect"
)

// Rp represents one repository
type Rp struct {
	// filled from config
	Name       string // name of the repository for crema
	DBName     string // name of the repository DB (defaults to Name)
	remotePath string // remote path of the repository
	signDB     bool   // sign the repo DB

	// determined from repo DB
	signed bool // is repo DB signed?

	// architecture
	arch string

	// directories
	chrootDir string
	localDir  string
}

// Rps is an array of repos
type Rps []Rp

// suffices
const (
	dbSuffix           = ".db"
	filesSuffix        = ".files"
	archiveSuffix      = ".tar.xz"
	dbArchiveSuffix    = dbSuffix + archiveSuffix
	filesArchiveSuffix = filesSuffix + archiveSuffix
)

// build modes
type buildMode int

const (
	buildModeAdd buildMode = iota
	buildModeUpdate
)

// newRepo creates an instance of Rp
func newRepo(name, dbName, remotePath string, signDB bool) (Rp, error) {
	arch, err := arch()
	if err != nil {
		return Rp{}, errors.Wrapf(err, "cannot create repo object for '%s'", name)
	}

	repo := Rp{
		Name:   name,
		DBName: dbName,
		remotePath: strings.Replace(
			strings.Replace(
				remotePath,
				"$arch",
				arch,
				-1),
			"$repo",
			name,
			-1),
		signDB: signDB,
		signed: false,
		arch:   arch}

	// make sure the relevant directories exist: chroot, local repo dir
	if err := repo.ensureDirs(); err != nil {
		return Rp{}, errors.Wrapf(err, "cannot create repo object for '%s'", name)
	}

	return repo, nil
}

// addToDB adds package files to the repository DB
func (repo *Rp) addToDB(files pkgfiles) error {
	// nothing to do if no files are given
	if len(files) == 0 {
		return nil
	}

	if repo.signed && !repo.signDB {
		// remove old .sig files that might be there (since the repo DB will not be
		// signed after the next update, the new DB files and the old .sig files
		// would not fit to each other)
		repo.removeDBSigs()
	}

	// assemble args for call of repo-add
	args := []string{"--remove", "--verify"}
	if repo.signDB {
		key, err := gpgkey()
		if err != nil {
			return errors.Wrapf(err, "cannot add packages to repo '%s'", repo.Name)
		}
		args = append(args, "--sign", "--key", key)
	}
	args = append(args, filepath.Join(repo.localDir, repo.DBName+dbArchiveSuffix))
	args = append(args, files.paths()...)

	// call repo-add
	_, stderr, err := exec.RunQuiet(
		[]string{},
		"repo-add",
		args...)
	if err != nil {
		fmt.Println(string(stderr))
		return errors.Wrapf(err, "could not add package files to repo '%s'", repo.Name)
	}

	return nil
}

// adjustPacmanConf takes a path to a template pacman.conf file. If it contains
// an entry for repo, this entry is removed. A new entry for repo is added
// pointing to the local copy of repo
func (repo Rp) adjustPacmanConf(path string) error {
	// read pacman.conf template into buffer
	content, err := os.ReadFile(path)
	if err != nil {
		return errors.Wrapf(err, "cannot read file '%s'", path)
	}

	// read buffer line by line and create modified buffer
	bufOld := bytes.NewBuffer(content)
	isRepo := false
	var bufNew string
	for {
		line, err := bufOld.ReadString('\n')
		if line == "" || (err != nil && err != io.EOF) {
			break
		}
		if len(line) > len(repo.DBName)+1 && line[:len(repo.DBName)+2] == "["+repo.DBName+"]" {
			isRepo = true
		}
		if isRepo && (line[0] != '[' && line[0] != '#' || len(line) > len(repo.DBName)+1 && line[:len(repo.DBName)+2] == "["+repo.DBName+"]") {
			continue
		}
		isRepo = false
		bufNew += line
	}

	// add repo entry pointing to local cache directory
	homeDir, err := os.UserHomeDir()
	if err != nil {
		return errors.Wrap(err, "cannot determine user home dir")
	}
	bufNew += "\n[" + repo.DBName + "]\n" + "SigLevel = Optional TrustAll\n" + "Server = file://" + homeDir + "/.cache/crema/repos/" + repo.Name + "\n"

	// write modified pacman.conf template to file
	if err = os.WriteFile(path, []byte(bufNew), 0644); err != nil {
		return errors.Wrapf(err, "cannot write file '%s'", path)
	}

	return nil
}

// buildAdd builds packages from a PKGBUILD file and adds them to repo. It does
// not take care of temporary data and locks.
func (repo Rp) buildAdd(mode buildMode, clean bool, sign bool, nochroot bool, pkgbuilds []Pkgbuild) error {
	if !nochroot {
		// create / update chroot environment
		err := repo.prepareChroot()
		if err != nil {
			return errors.Wrapf(err, "could not prepare chroot for repo '%s'", repo.Name)
		}
	}

	for _, pkgbuild := range pkgbuilds {
		// build packages from one PKGBUILD file and move files to repo dir
		files, err := pkgbuild.build(mode, repo.localDir, repo.chrootDir, sign, nochroot)
		if err != nil {
			msg.PrintErrorln("could not build package '%s': %v", pkgbuild, err)
			continue
		}

		// add package files to the repo DB
		if err = repo.addToDB(files); err != nil {
			msg.PrintErrorln("could not add packages to repo DB for package '%s'", pkgbuild)
		}
	}

	// clean chroot container is required by command line flag
	if clean {
		if err := repo.removeChroot(); err != nil {
			fmt.Println(err)
			msg.PrintErrorln("cannot remove chroot dir of repo '%s'", repo.Name)
			return errors.Wrapf(err, "cannot remove chroot dir of repo '%s'", repo.Name)
		}
	}

	return nil
}

// cleanup checks if the repo DB and the existing package and signature files
// are consistent to each other. If necessary, files are removed.
func (repo Rp) cleanup() error {
	msg.PrintInfoln("cleaning up '%s' ...", repo.Name)

	pkgs, err := repo.packages(pkgnames{})
	if err != nil {
		return errors.Wrapf(err, "cannot clean up repo '%s'", repo.Name)
	}
	files, err := repo.files()
	if err != nil {
		return errors.Wrapf(err, "cannot clean up repo '%s'", repo.Name)
	}

	// remove package files that do not correspond to a package in the repo DB
	for _, file := range files {
		if _, exists := pkgs[file.pkg()]; !exists {
			if err = os.Remove(filepath.Join(repo.localDir, string(file))); err != nil {
				return errors.Wrapf(err, "cannot remove file '%s'", file)
			}
			msg.PrintPlainln("removed '%s' since package is not in repo DB", file)
		}
	}

	// check consistency of .sig files and the corresponding package or repo
	// files
	pattern := filepath.Join(repo.localDir, "*.sig")
	matches, err := filepath.Glob(pattern)
	if err != nil {
		return errors.Wrapf(err, "cannot clean up repo '%s'", repo.Name)
	}
	for _, sig := range matches {
		// directories and symbolic links are not relevant
		fi, err := os.Lstat(sig)
		if err != nil {
			fmt.Println(err)
			msg.PrintErrorln("cannot retrieve info about '%s'", sig)
			continue
		}
		if fi.IsDir() || fi.Mode()&os.ModeSymlink == os.ModeSymlink {
			continue
		}

		// check if corresponding file for sig exists ...
		f := fp.PathTrunk(sig)
		exists, err := file.Exists(f)
		if err != nil {
			fmt.Println(err)
			msg.PrintErrorln("cannot check existence of file '%s'", f)
			continue
		}
		if exists {
			// ... if yes: verify signature file ...
			if !signatureOK(f) {
				msg.PrintErrorln("signature of '%s' not correct", f)
			}
			continue
		}
		// ... otherwise: remove .sig file
		if err = os.Remove(sig); err != nil {
			fmt.Println(err)
			msg.PrintErrorln("cannot remove file '%s'", sig)
			continue
		}
		msg.PrintPlainln("removed '%s' since corresponding package file does not exist", sig)
	}

	return nil
}

// createChroot creates a chroot container for the repository
func (repo Rp) createChroot() error {
	// make sure that the repository DB exists, otherwise makechrootpkg
	// will have a problem (in case of an empty repo, the DB does not exist).
	if err := repo.ensureDB(); err != nil {
		return errors.Wrapf(err, "could not ensure existence of DB for repo '%s'", repo.Name)
	}

	// determine paths of relevant makepkg.conf and pacman.conf
	makepkgConf, err := repo.makepkgConf()
	if err != nil {
		return errors.Wrapf(err, "cannot create chroot for repo '%s'", repo.Name)
	}
	pacmanConf, err := repo.pacmanConf()
	if err != nil {
		return errors.Wrapf(err, "cannot create chroot for repo '%s'", repo.Name)
	}

	// determine if distcc shall be used
	content, err := os.ReadFile(makepkgConf)
	if err != nil {
		return errors.Wrapf(err, "cannot create chroot since makepkg.conf file '%s' cannot be read", makepkgConf)
	}
	re := regexp.MustCompile(`\n[^#]*BUILDENV *= *[^\)]*[^!]+distcc`)
	distcc := (re.Find(content) != nil)

	// assemble mkarchroot parameters
	chroot := filepath.Join(repo.chrootDir, "root")
	params := []string{"-C", pacmanConf,
		"-M", makepkgConf,
		chroot,
		"base-devel"}
	if distcc {
		params = append(params, "distcc")
	}

	// create chroot
	msg.PrintInfoln("creating chroot for repo '%s' ...", repo.Name)
	if err := exec.RunVerbose(
		[]string{},
		"mkarchroot",
		params...,
	); err != nil {
		return errors.Wrap(err, "could not execute mkarchroot")
	}

	// execute prepchroot script if it exists
	exists, prepChroot := repo.prepChroot()
	if exists {
		msg.PrintInfoln("executing '%s' ...", filepath.Base(prepChroot))
		if err := exec.RunVerbose(
			[]string{},
			prepChroot,
			repo.Name,
			chroot,
		); err != nil {
			return errors.Wrapf(err, "could not execute prepchroot '%s'", prepChroot)
		}
	}

	// if the chroot is intended to be used for distributed builds, check if
	// distcc is installed. Display a warning if that's not the case
	if distcc {
		if _, _, err := exec.RunQuiet(
			[]string{},
			"pacman", "-Q", "distcc",
		); err != nil {
			msg.PrintWarnln("package 'distcc' must be installed on the system since otherwise distributed builds are not possible in the chroot")
		}
	}

	return nil
}

// deps extracts information about dependencies from the repo DB
func (repo *Rp) deps() (deps, error) {
	// extract names of packages that are contained in repo
	pkgs, err := repo.packages(pkgnames{})
	if err != nil {
		return deps{}, errors.Wrapf(err, "cannot extract dependencies for repo '%s'", repo.Name)
	}
	names := pkgs.names()

	// nothing to do if no packages could be determined
	if len(names) == 0 {
		return deps{}, nil
	}

	// extract dependencies
	deps := make(deps)
	if err := archiver.Walk(
		filepath.Join(repo.localDir, repo.DBName+dbArchiveSuffix),
		func(f archiver.File) error {
			if f.IsDir() || f.Name() != "desc" {
				return nil
			}
			const (
				sectionNone        = ""
				sectionDepends     = "%DEPENDS%"
				sectionMakeDepends = "%MAKEDEPENDS%"
				sectionFilename    = "%FILENAME%"
			)
			sc := bufio.NewScanner(f)
			sc.Split(bufio.ScanLines)
			section := sectionNone
			name := pkgname("")
			for sc.Scan() {
				line := sc.Text()
				if line == sectionNone || line == sectionFilename || line == sectionDepends || line == sectionMakeDepends {
					section = line
					continue
				}
				switch section {
				case sectionNone:
					continue
				case sectionFilename:
					file, err := newFile(filepath.Join(repo.localDir, line))
					if err != nil {
						return errors.Wrapf(err, "cannot parse dependencies of repo '%s'", repo.Name)
					}
					name = file.name()
				case sectionDepends, sectionMakeDepends:
					dep, err := nameFromDepStr(line)
					if err != nil {
						return errors.Wrapf(err, "cannot parse dependencies of repo '%s'", repo.Name)
					}
					if _, exists := names[dep]; exists {
						if _, exists := deps[dep]; !exists {
							deps[dep] = make(pkgnameMap)
						}
						if _, exists = deps[dep][name]; !exists {
							deps[dep][name] = struct{}{}
						}
					}
				}
			}
			return nil
		},
	); err != nil {
		return deps, errors.Wrapf(err, "cannot extract dependencies for repo '%s'", repo.Name)
	}

	return deps, nil
}

// download downloads remote repository data to the local repo directory
func (repo *Rp) download() error {
	msg.PrintInfoln("downloading '%s' from '%s' ...", repo.Name, repo.remotePath)

	// download repository data by executing "rsync"
	_, stderr, err := exec.RunQuiet(
		[]string{},
		"rsync",
		"-a",
		"--delete",
		repo.remotePath+"/",
		repo.localDir)
	if err != nil {
		return errors.Wrapf(err, "cannot download repo '%s': %s", repo.Name, string(stderr))
	}

	// set signed
	if repo.signed, err = file.Exists(filepath.Join(repo.localDir, repo.DBName+".db.sig")); err != nil {
		return errors.Wrapf(err, "cannot download repo '%s'", repo.Name)
	}

	return nil
}

// ensureDB checks if the repository DB exists in the local repo dir. If it
// doesn't exist, an empty DB is created
func (repo *Rp) ensureDB() error {
	exists, err := repo.existsDB()
	if err != nil {
		return errors.Wrapf(err, "cannot ensure DB for repo '%s'", repo.Name)
	}
	if !exists {
		msg.PrintInfoln("creating empty repository DB ...")
		_, stderr, err := exec.RunQuiet(
			[]string{},
			"repo-add",
			"-n",
			"-R",
			filepath.Join(repo.localDir, repo.DBName+dbArchiveSuffix),
		)
		if err != nil {
			return errors.Wrapf(err, "could not create DB for repo '%s': %s", repo.Name, string(stderr))
		}
	}
	return nil
}

// ensureDirs checks if the required directories exist for repo (chroot dir,
// local repo dir). If they don't exist, they are created
func (repo *Rp) ensureDirs() error {
	cacheDir, err := cacheDir()
	if err != nil {
		return errors.Wrap(err, "cannot ensure dirs")
	}

	// chroot dir
	repo.chrootDir = filepath.Join(cacheDir, chrootDirName, repo.Name)
	if err := file.CheckMkdir(repo.chrootDir, 0755); err != nil {
		return errors.Wrapf(err, "cannot create chroot directory for repo '%s", repo.Name)
	}
	// local repo dir
	repo.localDir = filepath.Join(cacheDir, repoDirName, repo.Name)
	if err := file.CheckMkdir(repo.localDir, 0755); err != nil {
		return errors.Wrapf(err, "cannot create local directory for repo '%s", repo.Name)
	}

	return nil
}

// existsDB check if the repo DB exists
func (repo Rp) existsDB() (bool, error) {
	exists, err := file.Exists(filepath.Join(repo.localDir, repo.DBName+dbArchiveSuffix))
	if err != nil {
		return false, errors.Wrapf(err, "cannot check for existence of DB of repo '%s'", repo.Name)
	}
	return exists, nil
}

// files returns the package files of repo
func (repo Rp) files() (pkgfiles, error) {
	// check if repo DB exists. If it doesn't exist: nothing to do
	exists, err := repo.existsDB()
	if !exists {
		return pkgfiles{}, errors.Wrapf(err, "cannot determine package file of repo '%s'", repo.Name)
	}

	// determine all package files
	pattern := filepath.Join(repo.localDir, "*-*-*-*.pkg.tar.*")
	matches, err := filepath.Glob(pattern)
	if err != nil {
		return pkgfiles{}, errors.Wrapf(err, "cannot determine matches for pattern '%s'", pattern)
	}
	var files pkgfiles
	for _, match := range matches {
		if !reFile.MatchString(match) {
			continue
		}
		files = append(files, pkgfile(match))
	}
	return files, nil
}

// infos returns information about the packages whose name is contained in
// names. If names is empty, information about all packages that are
// contained in repo are returned.
// The results is returned in two different flavors:
// (1) as an pkginfo array sorted ascending by package name
// (2) as a map: map[pkgname]pkginfo
func (repo Rp) infos(names pkgnames) (pkginfos, pkginfoMap, error) {
	strs, err := repo.packages(names)
	if err != nil {
		return pkginfos{}, pkginfoMap{}, errors.Wrapf(err, "cannot extract packages from DB of '%s'", repo.Name)
	}
	infoArr, infoMap := strs.infos()
	return infoArr, infoMap, nil
}

// makepkgConf returns the path to the makepkf.conf file that is supposed to be
// used as makepkg.conf in the chroots.
// makepkgConf checks the existence of files in the following sequence. The path
// of the first file that exists is returned:
// - ~/.config/crema/makepkg-<REPOSITORY>.conf
// - ~/.config/crema/makepkg.conf
// - /usr/share/devtools/makepkg-<ARCHITECTURE>.conf
// - /etc/makepkg.conf
func (repo Rp) makepkgConf() (string, error) {
	// check for ~/.config/crema/makepkg-<REPOSITORY>.conf and - if necessary -
	// ~/.config/crema/makepkg.conf
	cfgDir, err := cfgDir()
	if err != nil {
		fmt.Println(err)
		msg.PrintWarnln("could not determine config dir")
	} else {
		// check for ~/.config/crema/makepkg-<REPOSITORY>.conf
		makepkgConf := filepath.Join(cfgDir, "makepkg-"+repo.Name+".conf")
		exists, err := file.Exists(makepkgConf)
		if err != nil {
			fmt.Println(err)
			msg.PrintWarnln("cannot check existence of file '%s'", makepkgConf)
		} else if exists {
			return makepkgConf, nil
		}
		// check for ~/.config/crema/makepkg.conf
		makepkgConf = filepath.Join(cfgDir, "makepkg.conf")
		exists, err = file.Exists(makepkgConf)
		if err != nil {
			fmt.Println(err)
			msg.PrintWarnln("cannot check existence of file '%s'", makepkgConf)
		} else if exists {
			return makepkgConf, nil
		}
	}

	// check for /usr/share/devtools/makepkg-<ARCHITECTURE>.conf
	makepkgConf := "/usr/share/devtools/makepkg-" + repo.arch + ".conf"
	exists, err := file.Exists(makepkgConf)
	if err != nil {
		fmt.Println(err)
		msg.PrintWarnln("cannot check existence of file '%s'", makepkgConf)
	} else if exists {
		return makepkgConf, nil
	}

	// check for /etc/makepkg.conf
	makepkgConf = "/etc/makepkg.conf"
	exists, err = file.Exists(makepkgConf)
	if err != nil {
		fmt.Println(err)
		msg.PrintWarnln("cannot check existence of file '%s'", makepkgConf)
	} else if exists {
		return makepkgConf, nil
	}

	return "", fmt.Errorf("could not determine makepkg.conf for repo '%s'", repo.Name)
}

// packages returns a map of packages (i.e., package strings of the form
// <pkg-name>-<pkg-version>-<rel-no>) for each package whose name is contained
// in names. If names is empty, package strings for all packages contained in
// the repo DB are returned
func (repo Rp) packages(names pkgnames) (pkgMap, error) {
	// nothing to do if no repo DB exists
	exists, err := repo.existsDB()
	if err != nil {
		return pkgMap{}, errors.Wrapf(err, "cannot extract packages for repo '%s'", repo.Name)
	}
	if !exists {
		return pkgMap{}, nil
	}

	// determine packages
	pkgs := make(pkgMap)
	if err := archiver.Walk(
		filepath.Join(repo.localDir, repo.DBName+dbArchiveSuffix),
		func(f archiver.File) error {
			if !f.IsDir() {
				return nil
			}
			pkg, err := newPkg(f.Name())
			if err != nil {
				return errors.Wrapf(err, "cannot extract package string from '%s'", f.Name())
			}

			if len(names) == 0 || reflect.Contains(names, newInfo(pkg).name) {
				pkgs[pkg] = struct{}{}
			}
			return nil
		},
	); err != nil {
		return pkgMap{}, errors.Wrapf(err, "cannot retrieve package strings for repo '%s'", repo.Name)
	}

	return pkgs, nil
}

// pacmanConf returns the path of the pacman.conf file that is supposed to be
// used in chroots.
// For this, it checks the existence of files in the following sequence:
// - ~/.config/crema/pacman-<REPOSITORY>.conf
// - ~/.config/crema/pacman.conf
// - /etc/pacman.conf
// The first file in that sequence that exists is then taken as template. It is
// modified so that it contains an entry
//
//	[REPOSITORY]
//	SigLevel = Optional TrustAll
//	Server = file://<LOCAL PATH OF REPOSITORY>
//
// Then the modified file is stored in <tmpdir>/pacman.conf. That path is
// returned by pacmanConf and used as pacman.conf for the chroot creation
func (repo *Rp) pacmanConf() (string, error) {
	cfgDir, err := cfgDir()
	if err != nil {
		fmt.Println(err)
		msg.PrintWarnln("could not determine config dir")
	}

	// get path of relevant pacman.conf
	pacmanConf := filepath.Join(cfgDir, "pacman-"+repo.Name+".conf")
	exists, err := file.Exists(pacmanConf)
	if err != nil {
		fmt.Println(err)
		msg.PrintWarnln("cannot check existence of file '%s'", pacmanConf)
	} else if !exists {
		pacmanConf = filepath.Join(cfgDir, "pacman.conf")
		exists, err = file.Exists(pacmanConf)
		if err != nil {
			fmt.Println(err)
			msg.PrintWarnln("cannot check existence of file '%s'", pacmanConf)
		} else if !exists {
			pacmanConf = "/etc/pacman.conf"
			exists, err = file.Exists(pacmanConf)
			if err != nil {
				fmt.Println(err)
				msg.PrintWarnln("cannot check existence of file '%s'", pacmanConf)
			} else if !exists {
				return "", fmt.Errorf("cannot determine pacman.conf")
			}
		}
	}

	// copy pacman.conf to temp dir and use it as template
	tmpDir, err := tmpDir()
	if err != nil {
		return "", errors.Wrapf(err, "cannot create temp dir for repo '%s'", repo.Name)
	}
	newPacmanConf := filepath.Join(tmpDir, "pacman.conf")
	if err = file.Copy(pacmanConf, newPacmanConf); err != nil {
		return "", errors.Wrapf(err, "cannot copy '%s' to temp dir", pacmanConf)
	}

	// modify template file
	if err = repo.adjustPacmanConf(newPacmanConf); err != nil {
		return "", errors.Wrapf(err, "cannot adjust template pacman.conf '%s'", pacmanConf)
	}

	return newPacmanConf, nil
}

// prepChroot returns the path of a program that is executed after a new chroot
// was created (if such a script exists). If it exists, the first return value
// is true, otherwise false.
// For this, it checks the existence of files in the following sequence:
// - ~/.config/crema/prepchroot-<REPOSITORY>
// - ~/.config/crema/prepchroot
// The path of the first file in that sequence that exists is returned
func (repo *Rp) prepChroot() (bool, string) {
	cfgDir, err := cfgDir()
	if err != nil {
		fmt.Println(err)
		msg.PrintWarnln("could not determine if prepchroot script exists")
		return false, ""
	}

	// check existence of prepchroot-<REPOSITORY>
	prepChroot := filepath.Join(cfgDir, "prepchroot-"+repo.Name)
	exists, err := file.Exists(prepChroot)
	if err != nil {
		fmt.Println(err)
		msg.PrintWarnln("cannot check existence of file '%s'", prepChroot)
		return false, ""
	}
	if exists {
		return true, prepChroot
	}

	// check existence of prepchroot
	prepChroot = filepath.Join(cfgDir, "prepchroot")
	exists, err = file.Exists(prepChroot)
	if err != nil {
		fmt.Println(err)
		msg.PrintWarnln("cannot check existence of file '%s'", prepChroot)
		return false, ""
	}
	if exists {
		return true, prepChroot
	}
	return false, ""
}

// prepareChroot creates the chroot environment for the repo, if it does not
// exist already. If it already exists, it is being updated
func (repo *Rp) prepareChroot() error {
	// check if chroot already exists
	exists, err := file.Exists(filepath.Join(repo.chrootDir, "root", ".arch-chroot"))
	if err != nil {
		return errors.Wrapf(err, "cannot prepare chroot for repo '%s'", repo.Name)
	}

	// if chroot doesn't exist: create it
	if !exists {
		return repo.createChroot()
	}

	// if it exists: update it
	msg.PrintInfoln("updating chroot for repo '%s' ...", repo.Name)
	if err := exec.RunVerbose(
		[]string{},
		"arch-nspawn",
		filepath.Join(repo.chrootDir, "root"),
		"--bind-ro="+repo.localDir,
		"pacman", "-Syu", "--noconfirm",
	); err != nil {
		return errors.Wrapf(err, "could not update chroot for repo '%s'", repo.Name)
	}

	return nil
}

// removeDBSigs removes .sig files of the repo DB and prints a warning if DB
// was signed, but SignDB is deactivated.
func (repo *Rp) removeDBSigs() {
	// nothing to do if repo DB was not signed
	if !repo.signed {
		return
	}

	// retrieve all .sig files that have to be removed
	var (
		matches []string
		err     error
	)
	pattern1 := filepath.Join(repo.localDir, repo.DBName+".db*.sig")
	pattern2 := filepath.Join(repo.localDir, repo.DBName+".files*.sig")
	if matches, err = file.GlobOr([]string{pattern1, pattern2}); err != nil {
		msg.PrintErrorln("cannot determine matches for pattern '%s' and '%s'", pattern1, pattern2)
		return
	}

	if len(matches) > 0 {
		for _, match := range matches {
			if err = os.Remove(match); err != nil {
				fmt.Println(err)
				msg.PrintErrorln("cannot remove file '%s'", match)
			}
		}
		if !repo.signDB {
			msg.PrintWarnln("repo DB was signed but will not be signed again - signatures removed")
		}
	}
}

// removeChroot removes the chroot container of repo
func (repo Rp) removeChroot() error {
	_, stderr, err := exec.RunQuiet(
		[]string{},
		"sudo",
		"rm", "-rdf",
		repo.chrootDir,
	)
	if err != nil {
		fmt.Println(string(stderr))
		return err
	}
	return nil
}

// removePkgs removes the packages whose name is in names from repo
func (repo Rp) removePkgs(names pkgnames) error {
	// nothing to do if there is nothing to be removed
	if len(names) == 0 {
		return nil
	}

	if repo.signed && !repo.signDB {
		// remove old .sig files that might be there (since the repo DB will not be
		// signed after the next update, the new DB files and the old .sig files
		// would not fit to each other)
		repo.removeDBSigs()
	}

	// assemble arguments for repo-remove
	args := []string{"--verify"}
	if repo.signDB {
		key, err := gpgkey()
		if err != nil {
			return errors.Wrapf(err, "cannot remove packages from repo '%s'", repo.Name)
		}
		args = append(args, "-s", "-k", key)
	}
	args = append(args, filepath.Join(repo.localDir, repo.DBName+dbArchiveSuffix))
	for _, name := range names {
		args = append(args, string(name))
	}

	// execute repo-remove
	_, stderr, err := exec.RunQuiet(
		[]string{},
		"repo-remove",
		args...,
	)
	if err != nil {
		return errors.Wrapf(fmt.Errorf("%s", string(stderr)), "could not remove packages from repo '%s'", repo.Name)
	}

	// remove package files
	for _, name := range names {
		file, err := name.file(repo.localDir)
		if err != nil {
			msg.PrintErrorln("%v", err)
			continue
		}
		if err := file.remove(); err != nil {
			return errors.Wrapf(err, "cannot remove files of package '%s'", name)
		}
		msg.PrintPlainln("removed '%s'", string(file))
	}

	return nil
}

// sign signs repo DB files
func (repo *Rp) sign() (err error) {
	// sign archive files
	if err := sign(filepath.Join(repo.localDir, repo.DBName+dbArchiveSuffix)); err != nil {
		return errors.Wrapf(err, "cannot sign DB archive of repo '%s'", repo.Name)
	}
	if err := sign(filepath.Join(repo.localDir, repo.DBName+filesArchiveSuffix)); err != nil {
		return errors.Wrapf(err, "cannot sign files archive of repo '%s'", repo.Name)
	}
	// create sym links to signature files
	if err := os.Symlink(
		filepath.Join(repo.localDir, repo.DBName+dbArchiveSuffix+".sig"),
		filepath.Join(repo.localDir, repo.DBName+dbSuffix+".sig"),
	); err != nil {
		return errors.Wrapf(err, "cannot create sym link to signature file of DB archive of repo '%s'", repo.Name)
	}
	if err := os.Symlink(
		filepath.Join(repo.localDir, repo.DBName+filesArchiveSuffix+".sig"),
		filepath.Join(repo.localDir, repo.DBName+filesSuffix+".sig"),
	); err != nil {
		return errors.Wrapf(err, "cannot create sym link to signature file of files archive of repo '%s'", repo.Name)
	}

	return nil
}

// upload uploads repository data from the local to the remote directory.
func (repo *Rp) upload() (err error) {
	msg.PrintInfoln("uploading '%s' to '%s' ...", repo.Name, repo.remotePath)

	// upload repository data by executing "rsync"
	var stderr []byte
	_, stderr, err = exec.RunQuiet(
		[]string{},
		"rsync",
		"-a",
		"--delete",
		"--no-perms",
		repo.localDir+"/",
		repo.remotePath)
	if err != nil {
		fmt.Println(string(stderr))
		err = errors.Wrapf(err, "cannot upload repo '%s'", repo.Name)
		return
	}

	return
}
