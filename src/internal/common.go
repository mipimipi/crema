package crema

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strconv"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"gitlab.com/go-utilities/exec"
	"gitlab.com/go-utilities/file"
	"gitlab.com/go-utilities/system"
)

// repo config
type rpCfg = struct {
	DBName     string
	RemotePath string
	SignDB     bool
}
type repoCfgMap = map[string]rpCfg

// config file for repositories. crema knows only these repositories
const repoCfgFileName = "repos.conf"

// sub directory name for crema
const cremaDirName = "crema"

// names of sub directories of the data directory (which is typically
// ~/.cache/crema)
const repoDirName = "repos"
const lockDirName = "locks"
const tmpDirName = "tmp"
const pkgbuildDirName = "pkgbuild"
const pkgDirName = "pkg"
const chrootDirName = "chroots"

// arch determines the architecture (e.g. "x86_64") of the system and maps it to
// the corresponding Arch Linux constants
func arch() (string, error) {
	// get architecture
	uname, err := system.Uname()
	if err != nil {
		return "", errors.Wrap(err, "cannot call 'uname'")
	}

	// map arch to the architecture constants of Arch Linux
	switch uname.Machine[:5] {
	case "armv8":
		return "aarch64", nil
	case "armv7":
		return "armv7h", nil
	case "armv6":
		return "armv6h", nil
	case "armv5":
		return "arm", nil
	}

	return uname.Machine, nil
}

// cacheDir returns the user cache dir (normally, that's ~/.cache)
func cacheDir() (string, error) {
	cacheDir, err := os.UserCacheDir()
	if err != nil {
		return "", errors.Wrap(err, "cannot determine user cache dir")
	}
	cacheDir = filepath.Join(cacheDir, cremaDirName)
	if err = file.CheckMkdir(cacheDir, 0755); err != nil {
		return "", errors.Wrapf(err, "cannot create dir '%s'", cacheDir)
	}
	return cacheDir, nil
}

// cfgDir returns the path of the crema config directory. Normally, that's
// ~/.config/crema.
func cfgDir() (string, error) {
	cfgDir, err := os.UserConfigDir()
	if err != nil {
		return "", errors.Wrap(err, "cannot determine user config dir")
	}
	return filepath.Join(cfgDir, cremaDirName), nil
}

// gpgkey checks if the environment variable GPGKEY is set. If it's not set,
// it's tried to extract its value from /etc/makepkg.conf. GPGKEY is needed
// to sign packages and/or repository DB's
func gpgkey() (string, error) {
	key := os.Getenv("GPGKEY")
	if len(key) == 0 {
		// try to extract GPGKEY from /etc/makepkg.conf:
		// - read /etc/makepkg.conf
		content, err := os.ReadFile("/etc/makepkg.conf")
		if err != nil {
			return "", errors.Wrap(err, "could not determine GPGKEY from /etc/makepkg.conf")
		}
		r := regexp.MustCompile(`GPGKEY=(.*)`)
		match := r.FindSubmatch(content)
		// - get first submatch (if it exists)
		if len(match) > 1 {
			key = string(match[1])
		}
		if len(key) == 0 {
			return "", fmt.Errorf("environment variable GPGKEY is not set")
		}
	}
	return key, nil
}

// lockDir checks if the lock directory crema exists (normally, that's
// ~/.cache/crema/locks). If it doesn't exist, it's being created. lockDir
// returns the path of the directory.
func lockDir() (string, error) {
	cacheDir, err := cacheDir()
	if err != nil {
		return "", errors.Wrap(err, "cannot create lock dir")
	}

	lockDir := filepath.Join(cacheDir, lockDirName)
	if err := file.CheckMkdir(lockDir, 0755); err != nil {
		return "", errors.Wrapf(err, "cannot create lock dir '%s'", lockDir)
	}
	return lockDir, nil
}

// pkgbuildDir returns the path of the temporary directory to store PKGBUILD
// files from AUR (normally, that's ~/.cache/crema/tmp/<pid>/pkgbuild)
func pkgbuildDir() (string, error) {
	tmpDir, err := tmpDir()
	if err != nil {
		return "", errors.Wrap(err, "cannot determine pkgbuild dir")
	}

	return filepath.Join(tmpDir, pkgbuildDirName), nil
}

// pkgDir returns the path of the temporary directory to store package files
// that are created by makpkg or makechrootpkg (normally, that's
// ~/.cache/crema/tmp/<pid>/pkg)
func pkgDir() (string, error) {
	tmpDir, err := tmpDir()
	if err != nil {
		return "", errors.Wrap(err, "cannot determine pkg dir")
	}

	return filepath.Join(tmpDir, pkgDirName), nil
}

// rmTmpDir deletes the temporary directories of the current process
// (normally, that's ~/.cache/crema/tmp/<pid>
func removeTmpDir() error {
	tmpDir, err := tmpDir()
	if err != nil {
		return errors.Wrap(err, "cannot remove temp dir")
	}

	if err := os.RemoveAll(tmpDir); err != nil {
		return errors.Wrapf(err, "cannot remove tmp dir '%s'", tmpDir)
	}
	return nil
}

// repoCfg reads the crema config file (normally, that's
// ~/.config/crema/repos.cfg) and returns the content as map structure
func repoCfg() (repoCfgMap, error) {
	cfgDir, err := cfgDir()
	if err != nil {
		return repoCfgMap{}, errors.Wrap(err, "cannot read config since config dir cannot be determined")
	}

	cfg := make(repoCfgMap)

	// read config utilizing viper:
	// - read config file
	v := viper.New()
	v.SetConfigName(repoCfgFileName)
	v.SetConfigType("toml")
	v.AddConfigPath(cfgDir)
	if err = v.ReadInConfig(); err != nil {
		return repoCfgMap{}, errors.Wrap(err, "Repo config could not be read")
	}
	// - retrieve config data into map
	if err := v.Unmarshal(&cfg); err != nil {
		return repoCfgMap{}, errors.Wrap(err, "Repo config could not be unmarshalled")
	}
	// set optional DBName attribute (if no DB name is given, use the repo name
	// from config)
	for name, repoCfg := range cfg {
		if len(repoCfg.DBName) == 0 {
			cfg[name] = rpCfg{
				DBName:     name,
				RemotePath: repoCfg.RemotePath,
				SignDB:     repoCfg.SignDB,
			}
		}
	}
	return cfg, nil
}

// sign signs file with the gpg key stored in the environment variable GPGKEY
func sign(file string) error {
	// get key for signing
	key, err := gpgkey()
	if err != nil {
		return errors.Wrapf(err, "cannot sign package file '%s'", file)
	}

	// sign file
	_, _, err = exec.RunQuiet(
		[]string{},
		"gpg",
		"--yes",
		"-u", key,
		"--output", file+".sig",
		"--detach-sign",
		"--pinentry-mode=loopback",
		file,
	)
	if err != nil {
		return errors.Wrapf(err, "cannot sign package file '%s'", file)
	}

	return nil
}

// tmpDir creates the required temporary directory (incl. sub directories) for
// the current process if it does not yet exist (normally, they are under
// ~/.cache/crema/tmp/<pid>) and returns the path of that directory.
func tmpDir() (string, error) {
	cacheDir, err := cacheDir()
	if err != nil {
		return "", errors.Wrap(err, "cannot create temp dir")
	}

	tmpDir := filepath.Join(cacheDir, tmpDirName, strconv.Itoa(os.Getpid()))
	dirs := []string{
		filepath.Join(tmpDir, pkgbuildDirName),
		filepath.Join(tmpDir, pkgDirName)}

	for _, dir := range dirs {
		if err := file.CheckMkdir(dir, 0755); err != nil {
			return "", errors.Wrapf(err, "cannot create temp dir '%s'", dir)
		}
	}
	return tmpDir, nil
}

// isTmpSubDir checks if path is a sub directory of tmpDir/target. target must
// be a relative path
func isTmpSubDir(path string) (bool, error) {
	tmpDir, err := tmpDir()
	if err != nil {
		return false, errors.Wrapf(err, "cannot check if '%s' is sub dir of tmp dir", path)
	}

	// normalize directories
	tmpDir = filepath.Clean(tmpDir)
	path = filepath.Clean(path)

	return len(path) >= len(tmpDir) && tmpDir == path[:len(tmpDir)], nil
}

// copyToTmpDir copies the content of path recursively to the directory
// tmpDir/target/ABS(path) where ABS(path) is the absolute representation of
// path. target must be a relative path
func copyToTmpDir(path, target string) (string, error) {
	// target must be a relative path
	if filepath.IsAbs(target) {
		return "", fmt.Errorf("target '%s' is not a relative path", target)
	}

	absPath, err := filepath.Abs(path)
	if err != nil {
		return "", err
	}
	tmpDir, err := tmpDir()
	if err != nil {
		return "", errors.Wrapf(err, "cannot copy content of '%s' to tmp dir", path)
	}
	newPath := filepath.Join(tmpDir, target, absPath)
	if err := file.CheckMkdir(newPath, 0755); err != nil {
		return "", errors.Wrapf(err, "cannot copy content of '%s' to tmp dir", path)
	}
	_, stderr, err := exec.RunQuiet(
		[]string{},
		"cp",
		"-rf",
		absPath,
		filepath.Dir(newPath),
	)
	if err != nil {
		return "", errors.Wrapf(err, "cannot copy content of '%s' to '%s': %s", path, newPath, string(stderr))
	}

	return newPath, nil
}

// signatureOK return true if file+".sig" is the gpg signature of file.
// Otherwise false is returned
func signatureOK(file string) bool {
	_, _, err := exec.RunQuiet(
		[]string{},
		"gpg",
		"--verify", file+".sig",
		file,
	)
	return err == nil
}
