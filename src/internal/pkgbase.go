package crema

import (
	"fmt"
	"path/filepath"

	"github.com/pkg/errors"
	"gitlab.com/go-utilities/exec"
	"gitlab.com/go-utilities/msg"
)

// pkgbase is the name of a package base
type pkgbase string
type pkgbases []pkgbase

// pkgbuildFromAUR clones the AUR package base base to the local file system and
// creates a corresponding instance of pkgbuild (i.e., the resulting
// pkgbuild represents the directory where the downloaded PKGBUILD files is
// stored).
func (base pkgbase) pkgbuildFromAUR(ignoreArch bool) (Pkgbuild, error) {
	pkgbuildDir, err := pkgbuildDir()
	if err != nil {
		return "", errors.Wrapf(err, "cannot create pkgbuild dir for base '%s'", base)
	}

	// clone repository data via git
	_, stderr, err := exec.RunQuiet(
		[]string{},
		"git",
		"clone",
		aurURI+string(base)+".git",
		filepath.Join(pkgbuildDir, string(base)),
	)
	if err != nil {
		fmt.Println(string(stderr))
		return "", errors.Wrapf(err, "error cloning package base '%s'", base)
	}
	msg.PrintPlainln("cloned '%s'", base)

	return newPkgbuild(filepath.Join(pkgbuildDir, string(base)), ignoreArch)
}

// pkgbuildsFromAUR clones many AUR package bases to the local file system
func (bases pkgbases) pkgbuildsFromAUR(ignoreArch bool) (pkgbuilds []Pkgbuild) {
	// anything to do?
	if len(bases) == 0 {
		return
	}

	msg.PrintInfoln("retrieving PKGBUILD files from AUR ...")

	for _, base := range bases {
		if pkgbuild, err := base.pkgbuildFromAUR(ignoreArch); err == nil {
			pkgbuilds = append(pkgbuilds, pkgbuild)
		}
	}

	return
}
