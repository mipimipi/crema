package crema

import (
	"bufio"
	"bytes"
	"fmt"
	"strconv"

	"github.com/pkg/errors"
	"gitlab.com/go-utilities/exec"
)

// pkgversion is the version of a package
type pkgversion string

// lessThan returns true if v is newer than version according to vercmp
func (version pkgversion) lessThan(v pkgversion) (bool, error) {
	stdout, stderr, err := exec.RunQuiet(
		[]string{},
		"vercmp",
		string(version),
		string(v),
	)
	if err != nil {
		return false, errors.Wrap(fmt.Errorf("%s", string(stderr)), "cannot compare versions")
	}
	sc := bufio.NewScanner(bytes.NewReader(stdout))
	sc.Split(bufio.ScanLines)
	var i int
	for sc.Scan() {
		i, err = strconv.Atoi(sc.Text())
		if err != nil {
			return false, errors.Wrap(err, "cannot convert vercmp result")
		}
	}
	return (i < 0), nil
}
