package crema

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"regexp"

	"github.com/pkg/errors"
	"gitlab.com/go-utilities/exec"
	"gitlab.com/go-utilities/file"
	"gitlab.com/go-utilities/msg"
)

var reArch = regexp.MustCompile(`arch *= *\(.+\)`)

// Pkgbuild is the path of a directory where the PKGBUILD file is stored
type Pkgbuild string

// newPkgbuild takes a path of a directory where a PKGBUILD file is supposed to
// be located, checks if there's such a file, copies the directory to the tmp
// dir (if it's not yet located under tmp dir, which is the case for AUR
// packages), adjusts the arch array if --ignorearch is required and returns a
// corresponding instance of pkgbuild
func newPkgbuild(path string, ignoreArch bool) (Pkgbuild, error) {
	isDir, err := file.IsDir(path)
	if err != nil {
		return Pkgbuild(""), fmt.Errorf("'%s' is not a directory", path)
	}
	if !isDir {
		return Pkgbuild(""), fmt.Errorf("'%s' is no directory", path)
	}
	exists, err := file.Exists(filepath.Join(path, "PKGBUILD"))
	if err != nil {
		return Pkgbuild(""), errors.Wrapf(err, "cannot check existence of PKGBUILD file in '%s'", path)
	}
	if !exists {
		return Pkgbuild(""), fmt.Errorf("directory '%s' doesn't contain a PKGBUILD file", path)
	}

	pkgbuild := Pkgbuild(path)

	// copy directory to tmp dir if it's not yet a sub dir of tmp dir
	isSub, err := isTmpSubDir(path)
	if err != nil {
		return Pkgbuild(""), err
	}
	if !isSub {
		newPath, err := copyToTmpDir(path, pkgbuildDirName)
		if err != nil {
			return pkgbuild, err
		}
		pkgbuild = Pkgbuild(newPath)
	}

	// adjust arch array in PKGBUILD if required
	if ignoreArch {
		return pkgbuild.adjustArch()
	}

	return pkgbuild, nil
}

// adjustArch sets the arch array in the PKGBUILD file to "arch=(any)" to make
// makepkg to ignore the architectures there were originally specified
func (pkgbuild Pkgbuild) adjustArch() (Pkgbuild, error) {
	path := filepath.Join(string(pkgbuild), "PKGBUILD")

	// read PKGBUILD file
	old, err := os.ReadFile(path)
	if err != nil {
		return "", errors.Wrapf(err, "cannot read PKGBUILD file '%s'", path)
	}

	// adjust content of PKGBUILD file
	new := append(reArch.ReplaceAll(old, []byte{}), []byte("\narch=('any')")...)

	// write changes to PKGBUILD file
	if err = os.WriteFile(path, new, 0644); err != nil {
		return "", errors.Wrapf(err, "cannot write file '%s'", path)
	}
	return pkgbuild, nil
}

// build builds packages from a PKGBUILD file either utilizing makechrootpkg
// or makepkg. build signs the package files if required and returns a list
// of the packages files that have been created. The files are located in the
// local repo directory
func (pkgbuild Pkgbuild) build(mode buildMode, repoDir, chrootDir string, sign, nochroot bool) (pkgfiles, error) {
	// determine the packages that are planned to be built from the PKGBUILD file.
	// If list is empty: nothing to do
	pkglist, err := pkgbuild.packagelist()
	if err != nil {
		return pkgfiles{}, errors.Wrap(err, "cannot not build package '%s'")
	}
	if len(pkglist) == 0 {
		return pkgfiles{}, nil
	}

	msg.PrintInfoln("building from '%s'", pkgbuild)

	// build step: package files are stored in temporary pkg directory
	if nochroot {
		// build with makepkg
		if err := pkgbuild.makepkg(repoDir); err != nil {
			return pkgfiles{}, errors.Wrapf(err, "could not build package '%s' with makepkg", pkgbuild)
		}
	} else {
		// build with makechrootpkg
		if err := pkgbuild.makechrootpkg(repoDir, chrootDir); err != nil {
			return pkgfiles{}, errors.Wrapf(err, "could not build package '%s' with makechrootpkg", pkgbuild)
		}
	}

	// collect the files that have been created by the build step. It is also
	// checked if they really exist
	pkgDir, err := pkgDir()
	if err != nil {
		return pkgfiles{}, errors.Wrapf(err, "cannot collect package files '%s'", pkgbuild)
	}
	var files pkgfiles
	for _, n := range pkglist {
		file, err := n.file(pkgDir)
		if err != nil {
			msg.PrintErrorln("%v", err)
			continue
		}
		files = append(files, file)
	}

	// sign package files:
	// - If called during crema udate, a file is signed, if the package (maybe an
	//	 older version of it) was already signed in the past.
	// - If called during crema add, a file is only signed if required by the user.
	if mode == buildModeUpdate {
		tobesigned, err := files.signedVersionExists(repoDir)
		if err != nil {
			return pkgfiles{}, errors.Wrapf(err, "cannot sign packages of packages '%s'", pkgbuild)
		}
		for _, file := range tobesigned {
			msg.PrintPlainln("signing package '%s' since is was signed in the past", file.name())
		}
		if err := tobesigned.sign(); err != nil {
			return pkgfiles{}, errors.Wrapf(err, "could not build package '%s'", pkgbuild)
		}
	} else {
		if sign {
			if err := files.sign(); err != nil {
				return pkgfiles{}, errors.Wrapf(err, "could not build package '%s'", pkgbuild)
			}
		}
	}

	// remove old package files (i.e., files of older packages versions incl.
	// signature files)
	pkglist.removeFiles(repoDir)

	// move files from temporary pkg dir to local repo dir
	return files.moveToDir(repoDir)
}

// makechrootpkg builds packages from a PKGBUILD file with makechrootpkg and
// stores the package files in a temporary package directory (normally, that's
// ~/.cache/crema/tmp/<pid>/pkg)
func (pkgbuild Pkgbuild) makechrootpkg(repoDir, chrootDir string) error {
	// change to pkgbuild directory (and remember current directory)
	dir, err := os.Getwd()
	if err != nil {
		return errors.Wrap(err, "cannot determine current dir")
	}
	if err = os.Chdir(string(pkgbuild)); err != nil {
		return errors.Wrapf(err, "cannot change to dir '%s'", string(pkgbuild))
	}

	pkgDir, err := pkgDir()
	if err != nil {
		return errors.Wrap(err, "cannot determine pkg dir")
	}

	// execute makechrootpkg
	if err = exec.RunVerbose(
		[]string{"PKGDEST=" + pkgDir},
		"makechrootpkg",
		"-r", chrootDir,
		"-D", repoDir,
		"-u",
		"--", "-c", "--noconfirm", "--needed", "--syncdeps",
	); err != nil {
		return errors.Wrap(err, "error when executing makepkg")
	}

	// return to previous directory
	if err = os.Chdir(dir); err != nil {
		return errors.Wrapf(err, "cannot return to dir '%s'", string(pkgbuild))
	}

	return nil
}

// makepkg builds packages from a PKGBUILD file with makepkg and stores the
// package files in a temporary packages directory (normally, that's
// ~/.cache/crema/tmp/<pid>/pkg)
func (pkgbuild Pkgbuild) makepkg(repoDir string) error {
	// change to pkgbuild directory (and remember current directory)
	dir, err := os.Getwd()
	if err != nil {
		return errors.Wrap(err, "cannot determine current dir")
	}
	if err = os.Chdir(string(pkgbuild)); err != nil {
		return errors.Wrapf(err, "cannot change to dir '%s'", string(pkgbuild))
	}

	pkgDir, err := pkgDir()
	if err != nil {
		return errors.Wrap(err, "cannot determine pkg dir")
	}

	// execute makepkg
	if err = exec.RunVerbose(
		[]string{"PKGDEST=" + pkgDir},
		"env", "-u", "SHELLOPTS",
		"makepkg",
		"-c",
		"--noconfirm",
		"--needed",
		"--syncdeps",
	); err != nil {
		return errors.Wrap(err, "error when executing makepkg")
	}

	// return to previous directory
	if err = os.Chdir(dir); err != nil {
		return errors.Wrapf(err, "cannot return to dir '%s'", string(pkgbuild))
	}

	return nil
}

// packagelist retrieves the paths of the packages a PKGBUILD would deliver.
// That's done by executing "makepkg --packagelist" with PKGDEST set to the
// temporary package directory (normally, that's ~/.cache/crema/tmp/<pid>/pkg)
// as target dir for built packages
func (pkgbuild Pkgbuild) packagelist() (pkgnames, error) {
	// change to pkgbuild directory (and remember current directory)
	dir, err := os.Getwd()
	if err != nil {
		return pkgnames{}, errors.Wrapf(err, "cannot determine package list from PKGBUILD in '%s'", pkgbuild)
	}
	if err = os.Chdir(string(pkgbuild)); err != nil {
		return pkgnames{}, errors.Wrapf(err, "cannot determine package list from PKGBUILD in '%s'", pkgbuild)
	}

	pkgDir, err := pkgDir()
	if err != nil {
		return pkgnames{}, errors.Wrapf(err, "cannot determine package list from PKGBUILD in '%s'", pkgbuild)
	}

	// call "makepkg --packagelist"
	stdout, stderr, err := exec.RunQuiet(
		[]string{"PKGDEST=" + pkgDir},
		"makepkg",
		"--packagelist",
	)
	if err != nil {
		fmt.Println(string(stderr))
		return pkgnames{}, errors.Wrapf(err, "cannot determine package list from PKGBUILD in '%s'", pkgbuild)
	}
	sc := bufio.NewScanner(bytes.NewReader(stdout))
	sc.Split(bufio.ScanLines)
	var names pkgnames
	for sc.Scan() {
		file, err := newFile(sc.Text())
		if err != nil {
			return pkgnames{}, errors.Wrapf(err, "cannot determine package list from PKGBUILD in '%s'", pkgbuild)
		}
		names = append(names, file.name())
	}

	// return to previous directory
	if err = os.Chdir(dir); err != nil {
		return pkgnames{}, errors.Wrapf(err, "cannot determine package list from PKGBUILD in '%s'", pkgbuild)
	}

	return names, nil
}
