package crema

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"

	"github.com/pkg/errors"
	"gitlab.com/go-utilities/file"
	"gitlab.com/go-utilities/msg"
)

// lockedError is raised if a lock is tried to be written for a repository, but
// a lock from another processes exists already for that repository
type lockedError struct {
	pid  int // pid of the process that created the lock
	repo *Rp
	file string
}

// Error implements error interface for lockedError
func (l *lockedError) Error() string {
	return fmt.Sprintf("lock file '%s' exists: repository %s is locked by process %d", l.file, l.repo.Name, l.pid)
}

// pid extracts the pid from a lock file
func pid(file string) (int, error) {
	data, err := os.ReadFile(file)
	if err != nil {
		return 0, errors.Wrapf(err, "cannot extract pid from lock file %s", file)
	}
	pid, err := strconv.Atoi(string(data))
	if err != nil {
		return 0, errors.Wrapf(err, "cannot convert '%s' into pid", data)
	}
	return pid, nil
}

// lock writes a lock file for repo to ensure that only one process is
// changing the local repo dir at a time. lock returns an error if the repo is
// already locked by another process, otherwise it returns nil
func (repo *Rp) lock() error {
	lockDir, err := lockDir()
	if err != nil {
		return errors.Wrapf(err, "cannot lock repo '%s'", repo.Name)
	}
	lockFile := filepath.Join(lockDir, repo.Name)

	// check if file already exists
	exists, err := file.Exists(lockFile)
	if err != nil {
		return errors.Wrapf(err, "cannot check existence of file '%s'", lockFile)
	}
	if exists {
		pid, err := pid(lockFile)
		if err != nil {
			return errors.Wrap(err, "cannot check if lock file exists")
		}
		// nothing to do if the current process has already created a lock file
		if pid == os.Getpid() {
			msg.PrintWarnln("current process has already created a lock file")
			return nil
		}
		return &lockedError{
			pid:  pid,
			repo: repo,
			file: lockFile,
		}
	}

	// write lock
	file, err := os.Create(lockFile)
	if err != nil {
		return errors.Wrapf(err, "cannot create lock file '%s'", lockFile)
	}
	defer file.Close()
	_, err = file.WriteString(strconv.Itoa(os.Getpid()))
	if err != nil {
		return errors.Wrapf(err, "cannot write into lock file '%s'", lockFile)
	}

	return nil
}

// unlock removes the lock file for repo
func (repo *Rp) unlock() error {
	lockDir, err := lockDir()
	if err != nil {
		return errors.Wrapf(err, "cannot unlock repo '%s'", repo.Name)
	}
	lockFile := filepath.Join(lockDir, repo.Name)

	// check if file already exists
	exists, err := file.Exists(lockFile)
	if err != nil {
		return errors.Wrapf(err, "cannot check existence of file '%s'", lockFile)
	}
	// if that's not the case: nothing to do
	if !exists {
		msg.PrintWarnln("lock file '%s' does not exist though it should", lockFile)
		return nil
	}
	// check if pid fits
	pid, err := pid(lockFile)
	if err != nil {
		return errors.Wrapf(err, "cannot check if pid fits for unlocking repo '%s'", repo.Name)
	}
	if pid != os.Getpid() {
		return errors.Wrapf(err, "cannot unlock repo '%s' since it's locked by process %d", repo.Name, pid)
	}

	// finally, remove lock file
	err = os.Remove(lockFile)
	if err != nil {
		return errors.Wrapf(err, "cannot remove lock file '%s'", lockFile)
	}
	return nil
}
