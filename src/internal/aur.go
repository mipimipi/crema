package crema

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/go-utilities/msg"
)

// aurInfo takes the result data of a AUR web api call
type aurInfo struct {
	Version     int
	Type        string
	ResultCount int
	Results     []struct {
		ID             int
		Name           string
		PackageBaseID  int
		PackageBase    string
		Version        string
		Description    string
		URL            string
		NumVotes       int
		Popularity     float64
		OutOfDate      int
		Maintainer     string
		FirstSubmitted int
		LastModified   int
		URLPath        string
		MakeDepends    []string
		OptDepends     []string
		Provides       []string
		Replaces       []string
		License        []string
		Keywords       []string
	}
}

// AUR uris
const (
	aurURI     = "https://aur.archlinux.org/"
	aurInfoURI = aurURI + "rpc/?v=5&type=info"
)

// infoFromAUR retrieves information about packages from AUR
func infoFromAUR(names pkgnames) (ai aurInfo, err error) {
	// no api call necessary if no package names have been given
	if len(names) == 0 {
		return
	}

	// assemble uri
	uri := aurInfoURI
	for _, name := range names {
		uri += ("&arg[]=" + string(name))
	}

	// call rpc api
	r, err := http.Get(uri)
	if err != nil {
		msg.PrintErrorln("error calling AUR web api: %v", err)
		return
	}
	defer r.Body.Close()

	// check HTTP status
	if r.StatusCode >= http.StatusBadRequest {
		err = fmt.Errorf("HTTP error when calling AUR RPC interface: %s", r.Status)
		msg.PrintErrorln("HTTP error when calling AUR RPC interface: %s", r.Status)
		return
	}

	// decode JSON response into structure
	err = json.NewDecoder(r.Body).Decode(&ai)
	if err != nil {
		msg.PrintErrorln("cannot decode AUR response: %v", err)
	}

	return
}
