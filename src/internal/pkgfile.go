package crema

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"

	"github.com/pkg/errors"
	f "gitlab.com/go-utilities/file"
	"gitlab.com/go-utilities/msg"
)

// regular expressions. Package file is of the form
// "<pkg-name>-<pkg-version>-<rel-no>-<arch>.pkg.tar.<compression"
var (
	// package file path
	reFile = regexp.MustCompile(`^.*\/.+-[^-]+-[^-]+-[^\.]+\.pkg\.tar\.[^\.]+$`)
	// package name from package file path
	reNameFromFile = regexp.MustCompile(`^.*\/(.+)-[^-]+-[^-]+-[^\.]+\.pkg\.tar\.[^\.]+$`)
	// package name and version from package file path
	rePkgFromFile = regexp.MustCompile(`^.*\/(.+-[^-]+-[^-]+)-[^\.]+\.pkg\.tar\.[^\.]+$`)
	// everything from package file path except package name and version
	reAllExceptVersion = regexp.MustCompile(`^.*\/(.+)-[^-]+-[^-]+-([^\.]+\.pkg\.tar\.[^\.]+)$`)
)

// pkgfile is the path to a package file (a file of the form
// "<pkg-name>-<pkg-version>-<rel-no>-<arch>.pkg.tar.zst")
type pkgfile string
type pkgfiles []pkgfile

// newFile takes a string that is supposed to represent the file path of a
// package and converts it into a pkgfile
func newFile(s string) (pkgfile, error) {
	if !reFile.MatchString(s) {
		return pkgfile(""), fmt.Errorf("'%s' is not a valid package file (name)", s)
	}
	return pkgfile(s), nil
}

// moveToDir moves a pkg file (and related files such as a corresponding .sig
// file) to directory dir
func (file pkgfile) moveToDir(dir string) (pkgfile, error) {
	// check if dir is a directory
	isDir, err := f.IsDir(dir)
	if err != nil {
		return "", errors.Wrapf(err, "cannot check whether '%s' is a directory", dir)
	}
	if !isDir {
		return "", errors.Wrapf(err, "'%s' is not a directory", dir)
	}

	d, _ := filepath.Split(string(file))
	// nothing to do if file is already in the target dir
	if d == dir {
		return file, nil
	}
	// move files to target dir
	pattern := string(file) + "*"
	matches, err := filepath.Glob(pattern)
	if err != nil {
		return "", errors.Wrapf(err, "cannot determine matching files for pattern '%s'", pattern)
	}
	var newFile pkgfile
	for _, match := range matches {
		_, f := filepath.Split(match)
		dst := filepath.Join(dir, f)
		if match == string(file) {
			newFile = pkgfile(dst)
		}
		// move file
		err := os.Rename(match, dst)
		if err != nil {
			return "", errors.Wrapf(err, "cannot rename file '%s'", match)
		}
	}
	return newFile, nil
}

// moveToDir for an entire array of pkg files
func (files pkgfiles) moveToDir(dir string) (pkgfiles, error) {
	var dsts pkgfiles
	for _, file := range files {
		dst, err := file.moveToDir(dir)
		if err != nil {
			return pkgfiles{}, errors.Wrapf(err, "cannot move files to dir '%s'", dir)
		}
		dsts = append(dsts, dst)
	}
	return dsts, nil
}

// name creates an instance of pkgname from a package file (name)
func (file pkgfile) name() pkgname {
	a := reNameFromFile.FindStringSubmatch(string(file))
	if len(a) != 2 {
		panic(fmt.Sprintf("package file name '%s' has wrong format", file))
	}

	return pkgname(a[1])
}

// pkg creates an instance of pkg from a package file (name)
func (file pkgfile) pkg() pkg {
	a := rePkgFromFile.FindStringSubmatch(string(file))
	if len(a) != 2 {
		panic(fmt.Sprintf("package file name '%s' has wrong format", file))
	}

	return pkg(a[1])
}

// paths returns the paths of the pkg files as string array
func (files pkgfiles) paths() (paths []string) {
	for _, file := range files {
		paths = append(paths, string(file))
	}
	return
}

// remove removes a pkg file together with related files, such as a
// corresponding .sig file
func (file pkgfile) remove() error {
	pattern := string(file) + "*"
	matches, err := filepath.Glob(pattern)
	if err != nil {
		return errors.Wrapf(err, "cannot remove file '%s' since no matches could be determined for pattern '%s'", file, pattern)
	}
	for _, match := range matches {
		err := os.Remove(match)
		if err != nil {
			fmt.Println(err)
			msg.PrintErrorln("cannot remove file '%s'", match)
		}
	}
	return nil
}

// sign signs a pkg file via gpg. I.e., a new .sig file is created
func (file pkgfile) sign() error {
	return sign(string(file))
}

// sign signs all files of an array
func (files pkgfiles) sign() error {
	for _, file := range files {
		if err := file.sign(); err != nil {
			return errors.Wrapf(err, "could not sign package file '%s'", file)
		}
	}
	return nil
}

// signedVersionExists returns the subset of files for which a signed version
// exists in dir. I.e., the file in dir representing the same or a different
// version of the package that one of the the input files represent
func (files pkgfiles) signedVersionExists(dir string) (pkgfiles, error) {
	// check if dir is a directory
	isDir, err := f.IsDir(dir)
	if err != nil {
		return pkgfiles{}, errors.Wrapf(err, "cannot check whether '%s' is a directory", dir)
	}
	if !isDir {
		return pkgfiles{}, errors.Wrapf(err, "'%s' is not a directory", dir)
	}

	var out pkgfiles
	for _, file := range files {
		a := reAllExceptVersion.FindStringSubmatch(string(file))
		if len(a) != 3 {
			continue
		}
		pattern := filepath.Join(dir, a[1]+"-*-*-"+a[2]+".sig")
		matches, err := filepath.Glob(pattern)
		if err != nil {
			return pkgfiles{}, errors.Wrapf(err, "cannot find matches for pattern '%s'", pattern)
		}
		if len(matches) > 0 {
			out = append(out, file)
		}
	}
	return out, nil
}
