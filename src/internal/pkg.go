package crema

import (
	"fmt"
	"regexp"
	"sort"
)

// regular expressions
var (
	// package string ("<pkg-name>-<pkg-version>-<rel-no>")
	rePkg = regexp.MustCompile(`^.*-[^-]+-[^-]+$`)
	// package name in package string ("<pkg-name>-<pkg-version>-<rel-no>")
	reNameFromPkg = regexp.MustCompile(`^(.*)-[^-]+-[^-]+$`)
)

// pkg represents a package name/version string of the form
// <pkg-name>-<pkg-version>-<rel-no>
type pkg string
type pkgMap map[pkg]struct{}

// newPkg expects a string of the form <pkg-name>-<pkg-version>-<rel-no> and
// converts it into a pkg
func newPkg(s string) (pkg, error) {
	if !rePkg.MatchString(s) {
		return pkg(""), fmt.Errorf("'%s' is not a valid package string", s)
	}

	return pkg(s), nil
}

// infos returns information about the packages in pkgs as pkginfo array and map
func (pkgs pkgMap) infos() (pkginfos, pkginfoMap) {
	infoArr := pkginfos{}
	infoMap := make(pkginfoMap)

	for pkg := range pkgs {
		info := newInfo(pkg)
		infoArr = append(infoArr, info)
		infoMap[info.name] = info
	}

	sort.Sort(infoArr)

	return infoArr, infoMap
}

// names returns the names of the packages from pkgs without version information
func (pkgs pkgMap) names() pkgnameMap {
	names := make(pkgnameMap)

	for pkg := range pkgs {
		a := reNameFromPkg.FindStringSubmatch(string(pkg))
		if len(a) != 2 {
			panic(fmt.Sprintf("package string '%s' has wrong format", pkg))
		}

		name := pkgname(a[1])
		if _, exists := names[name]; !exists {
			names[name] = struct{}{}
		}
	}

	return names
}
