package crema

import (
	"fmt"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
	"gitlab.com/go-utilities/file"
	"gitlab.com/go-utilities/msg"
)

// Add builds packages from a PKGBUILD files and adds them to repo. It also
// manages temporary data, locks and the download and upload from and to the
// remote directory of the repo
func (repo Rp) Add(clean bool, sign bool, nochroot bool, pkgbuilds []Pkgbuild) error {
	// tmp dirs have already been created to determine the input PKGBUILDS.
	// Therefore, the removal of these dirs must already be registered here
	defer func() { _ = removeTmpDir() }()

	// nothing to do if repo is initial
	if repo.IsNull() {
		return nil
	}

	// nothing to do if there are no PKGBUILD files
	if len(pkgbuilds) == 0 {
		msg.PrintMsgln("nothing to add: exit")
		return nil
	}

	// nothing to do if packages or repository DB are required to be signed but
	// GPGKEY is not set
	if sign || repo.signDB {
		_, err := gpgkey()
		if err != nil {
			fmt.Println(err)
			msg.PrintErrorln("cannot add packages to repo '%s' since DB must be signed but cannot", repo.Name)
			return errors.Wrapf(err, "cannot add packages to repo '%s' since DB must be signed but cannot", repo.Name)
		}
	}

	// since the local repository will be changed, a lock must be set
	if err := repo.lock(); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot create lock")
		return errors.Wrapf(err, "cannot add packages to repo '%s'", repo.Name)
	}
	defer func() { _ = repo.unlock() }()

	if err := repo.download(); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot add packages to repo '%s'", repo.Name)
		return errors.Wrapf(err, "cannot add packages to repo '%s'", repo.Name)
	}

	if err := repo.buildAdd(buildModeAdd, clean, sign, nochroot, pkgbuilds); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("could not add packages to repo '%s'", repo.Name)
		if errUpload := repo.upload(); errUpload != nil {
			fmt.Println(errUpload)
			msg.PrintErrorln("cannot upload packages to repo '%s'", repo.Name)
		}
		return errors.Wrapf(err, "cannot add packages to repo '%s'", repo.Name)
	}

	if err := repo.upload(); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot add packages to repo '%s'", repo.Name)
		return errors.Wrapf(err, "cannot add packages to repo '%s'", repo.Name)
	}

	return nil
}

// Cleanup cleans up repo, i.e. makes sure that the package files and the repo
// DB are consistent to each other
func (repo Rp) Cleanup() Rp {
	// nothing to do if repo is initial
	if repo.IsNull() {
		return repo
	}

	// since the local repository will be changed, a lock must be set
	if err := repo.lock(); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot create lock")
		return repo
	}
	defer func() { _ = repo.unlock() }()

	if err := repo.download(); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot cleanup repo '%s'", repo.Name)
		return Rp{}
	}

	if err := repo.cleanup(); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot cleanup repo '%s'", repo.Name)
	}

	if err := repo.upload(); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot cleanup repo '%s'", repo.Name)
		return Rp{}
	}

	return repo
}

// Clear deletes the chroot environment and/or the local dir/cache of repo
func (repo Rp) Clear(chroot bool, cache bool) error {
	// nothing to do if repo is initial
	if repo.IsNull() {
		return nil
	}

	if !chroot && !cache {
		msg.PrintMsgln("Please speficy what shall be cleared (--cache or --chroot). See documentation ('crema help clear')")
		return fmt.Errorf("missing argument: cannot clear repo '%s'", repo.Name)
	}

	// remove chroot environment
	if chroot {
		if err := repo.removeChroot(); err != nil {
			fmt.Println(err)
			msg.PrintErrorln("cannot remove chroot dir of repo '%s'", repo.Name)
			return errors.Wrapf(err, "cannot remove chroot dir of repo '%s'", repo.Name)
		}
		msg.PrintInfoln("chroot environment of '%s' removed", repo.Name)
	}

	// remove local repo dir
	if cache {
		if err := os.RemoveAll(repo.localDir); err != nil {
			fmt.Println(err)
			msg.PrintErrorln("cannot remove local cache of repo '%s'", repo.Name)
			return errors.Wrapf(err, "cannot remove local cache of repo '%s'", repo.Name)
		}
		msg.PrintInfoln("local cache of '%s' removed", repo.Name)
	}

	return nil
}

// IsNull returns true if a repository is initial
func (repo Rp) IsNull() bool {
	return repo.Name == ""
}

// IsRepo returns true if repo is configured in the config file
func IsRepo(repo string) (bool, error) {
	// read configuration
	cfg, err := repoCfg()
	if err != nil {
		msg.PrintErrorln("cannot check if '%s' is a valid repository", repo)
		return false, errors.Wrapf(err, "cannot check if '%s' is a valid repository", repo)
	}

	_, exists := cfg[repo]
	return exists, nil
}

// ListPackages lists all packages that are contained in a repository
func (repo Rp) ListPackages() Rp {
	if repo.IsNull() {
		return repo
	}

	if err := repo.download(); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot list packages of repo '%s'", repo.Name)
		return Rp{}
	}

	// header line
	s := "-"
	if repo.signed {
		s = "s"
	}
	fmt.Printf("%s  [%s]\n", s, repo.Name)

	// extract dependency information
	deps, err := repo.deps()
	if err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot list packages of repo '%s'", repo.Name)
		return Rp{}
	}

	infos, _, err := repo.infos(strToNames([]string{}))
	if err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot list packages of repo '%s'", repo.Name)
		return Rp{}
	}
	for _, info := range infos {
		signed, err := info.signed(repo)
		if err != nil {
			msg.PrintErrorln(fmt.Sprintf("%v", err))
			continue
		}
		s := "-"
		if signed {
			s = "s"
		}
		d := "-"
		if _, exists := deps[info.name]; exists {
			d = "d"
		}

		fmt.Printf("%s%s %s\n", s, d, info.string())
	}

	return repo
}

// ListRepos lists all repositories that are defined in the config file
func ListRepos() error {
	// read configuration
	cfg, err := repoCfg()
	if err != nil {
		msg.PrintErrorln("cannot list repositories")
		return errors.Wrap(err, "cannot list repositories")
	}

	// nothing to do if no repos are configured
	if len(cfg) == 0 {
		return nil
	}

	// print repo names separated by space
	isFirst := true
	for name := range cfg {
		if isFirst {
			isFirst = false
		} else {
			fmt.Printf(" ")
		}
		fmt.Printf("%s", name)
	}
	fmt.Printf("\n")

	return nil
}

// MkChroot creates a chroot container for the repository
func (repo Rp) MkChroot() error {
	// nothing to do if repo is initial
	if repo.IsNull() {
		return nil
	}

	// check if chroot already exists
	exists, err := file.Exists(filepath.Join(repo.chrootDir, "root", ".arch-chroot"))
	if err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot check existence of chroot for repo '%s'", repo.Name)
		return errors.Wrapf(err, "cannot create chroot for repo '%s'", repo.Name)
	}

	// if chroot exists: delete it
	if exists {
		if !msg.UserOK(fmt.Sprintf("A chroot for repo '%s' exists already. It is now being deleted", repo.Name)) {
			return errors.Wrapf(err, "cannot create chroot for repo '%s'", repo.Name)
		}
		if err := repo.removeChroot(); err != nil {
			fmt.Println(err)
			msg.PrintErrorln("cannot remove chroot dir of repo '%s'", repo.Name)
			return errors.Wrapf(err, "cannot create chroot for repo '%s'", repo.Name)
		}
		if err := repo.ensureDirs(); err != nil {
			fmt.Println(err)
			msg.PrintErrorln("cannot create chroot for repo '%s'", repo.Name)
			return errors.Wrapf(err, "cannot create chroot for repo '%s'", repo.Name)
		}
	}

	// create chroot
	if err := repo.createChroot(); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot create chroot for repo '%s'", repo.Name)
		return errors.Wrapf(err, "cannot create chroot for repo '%s'", repo.Name)
	}

	return nil
}

// PKGBUILDsFromAUR retrieves packages from AUR, stores the package files
// (particularly the PKGBUILD in a temporary directory). If ignoreArch is set,
// the arch array in PKGBUILD is modified. It returns rhe paths to the
// directories where the PKGBUILDs are stored
func PKGBUILDsFromAUR(names []string, ignoreArch bool) []Pkgbuild {
	return strToNames(names).existsInAUR().bases().pkgbuildsFromAUR(ignoreArch)
}

// PKGBUILDsFromLocal checks for every string in paths if it's the path of a
// directory that contains a PKGBUILD file. These directories are copied to the
// temporary directory and the arch array in PKBUILD is modified if ignoreArch
// is set. A list of these directories is returned
func PKGBUILDsFromLocal(paths []string, ignoreArch bool) (pkgbuilds []Pkgbuild) {
	for _, path := range paths {
		pkgbuild, err := newPkgbuild(path, ignoreArch)
		if err != nil {
			msg.PrintErrorln("skipping '%s': %v", path, err)
			continue
		}
		pkgbuilds = append(pkgbuilds, pkgbuild)
	}
	return pkgbuilds
}

// Remove removes the named packages from the repository
func (repo Rp) Remove(noconfirm bool, names []string) error {
	// nothing to do if repo is initial
	if repo.IsNull() {
		return nil
	}

	// since the local repository will be changed, a lock must be set
	if err := repo.lock(); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot create lock")
		return errors.Wrapf(err, "cannot remove package(s) from repo '%s'", repo.Name)
	}
	defer func() { _ = repo.unlock() }()

	if err := repo.download(); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot remove package(s) from repo '%s'", repo.Name)
		return errors.Wrapf(err, "cannot remove package(s) from repo '%s'", repo.Name)
	}

	// nothing to do if there's no repo DB
	exists, err := repo.existsDB()
	if err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot remove packages from repo '%s'", repo.Name)
		return nil
	}
	if !exists {
		msg.PrintWarnln("repository '%s' doesn't (yet) have a DB: Nothing to remove", repo.Name)
		return nil
	}

	// ask user for OK if packages are intended to be removed that other
	// packages of this repo depend on
	namesToBeRemoved := pkgnames{}
	deps, err := repo.deps()
	if err != nil {
		return errors.Wrapf(err, "cannot remove package(s) from repo '%s'", repo.Name)
	}
	for _, dep := range strToNames(names) {
		if names, exists := deps[dep]; exists {
			s := fmt.Sprintf("package '%s' is required by", dep)
			for name := range names {
				s += fmt.Sprintf(" '%s'", name)
			}
			msg.PrintWarnln(s)
			if noconfirm || msg.UserOK("Do you really want to remove the package") {
				namesToBeRemoved = append(namesToBeRemoved, dep)
			}
		} else {
			namesToBeRemoved = append(namesToBeRemoved, dep)
		}
	}

	if err := repo.removePkgs(namesToBeRemoved); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("could not remove packages from repo '%s'", repo.Name)
		if errUpload := repo.upload(); errUpload != nil {
			fmt.Println(errUpload)
			msg.PrintErrorln("cannot upload packages to repo '%s'", repo.Name)
		}
		return errors.Wrapf(err, "cannot remove package(s) from repo '%s'", repo.Name)
	}

	if err := repo.upload(); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot remove package(s) from repo '%s'", repo.Name)
		return errors.Wrapf(err, "cannot remove package(s) from repo '%s'", repo.Name)
	}

	return nil
}

// Repo returns a repo with the config data for the repository "name". If the
// config doesn't contain data for that repository, an empty repo is returned.
func Repo(name string) Rp {
	cfg, err := repoCfg()
	if err != nil {
		return Rp{}
	}

	r, ok := cfg[name]
	if !ok {
		return Rp{}
	}
	repo, err := newRepo(name, r.DBName, r.RemotePath, r.SignDB)
	if err != nil {
		fmt.Println(err)
		msg.PrintErrorln("could not create repo object for '%s'", name)
		return Rp{}
	}

	return repo
}

// Repos reads the config file for repositories (typically, that's
// ~/.config/crema/repos.conf) and creates an array of repos whose names are
// contained in names. If names is empty, an array of all configured repos is
// returned
func ReposByNames(names []string) (repos Rps) {
	cfg, err := repoCfg()
	if err != nil {
		return
	}

	if len(names) == 0 {
		for name, repoCfg := range cfg {
			repo, err := newRepo(
				name,
				repoCfg.DBName,
				repoCfg.RemotePath,
				repoCfg.SignDB)
			if err != nil {
				fmt.Println(err)
				msg.PrintErrorln("could not create repo object for '%s'", name)
				continue
			}
			repos = append(repos, repo)
		}
		return
	}

	for _, name := range names {
		repoCfg, exists := cfg[name]
		if !exists {
			msg.PrintErrorln("repo '%s' is not configured", name)
			continue
		}
		repo, err := newRepo(
			name,
			repoCfg.DBName,
			repoCfg.RemotePath,
			repoCfg.SignDB)
		if err != nil {
			fmt.Println(err)
			msg.PrintErrorln("could not create repo object for '%s'", name)
			continue
		}
		repos = append(repos, repo)
	}

	return
}

// Sign signs the named packages or (if names is empty) all unsigned packages
// of the repository
func (repo Rp) Sign(names []string) error {
	// nothing to do if repo is initial
	if repo.IsNull() {
		return nil
	}

	// since the local repository will be changed, a lock must be set
	if err := repo.lock(); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot create lock")
		return errors.Wrapf(err, "cannot sign packages of repo '%s'", repo.Name)
	}
	defer func() { _ = repo.unlock() }()

	if len(names) == 0 {
		msg.PrintInfoln("signing all packages of repo '%s'", repo.Name)
	} else {
		msg.PrintInfoln("signing selected packages of repo '%s'", repo.Name)
	}

	if err := repo.download(); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot sign packages of repo '%s'", repo.Name)
		return errors.Wrapf(err, "cannot sign packages of repo '%s'", repo.Name)
	}

	// nothing to do if there's no repo DB
	exists, err := repo.existsDB()
	if err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot sign packages of repo '%s'", repo.Name)
		return nil
	}
	if !exists {
		msg.PrintWarnln("repository '%s' doesn't (yet) have a DB: Nothing to sign", repo.Name)
		return nil
	}

	// get relevant package files
	var files pkgfiles
	if len(names) == 0 {
		files, err = repo.files()
		if err != nil {
			return errors.Wrapf(err, "cannot sign files of repo '%s'", repo.Name)
		}
	} else {
		files, err = strToNames(names).files(repo.localDir)
		if err != nil {
			return errors.Wrapf(err, "cannot sign files of repo '%s'", repo.Name)
		}

	}

	// sign package files
	if len(files) > 0 {
		if err := files.sign(); err != nil {
			return errors.Wrapf(err, "cannot sign files of repo '%s'", repo.Name)
		}
		msg.PrintInfoln("signed packages")
	}

	// sign repo DB
	if repo.signDB && !repo.signed {
		if err := repo.sign(); err != nil {
			return err
		}
		msg.PrintInfoln("signed repo DB")
	}

	if err := repo.upload(); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot sign packages of repo '%s'", repo.Name)
		return errors.Wrapf(err, "cannot sign packages of repo '%s'", repo.Name)
	}

	return nil
}

// Update updates outdated AUR packages whose name is contained in names. If
// names is empty, all outdated AUR packages of repo will be updated.
func (repo Rp) Update(nochroot, noconfirm, ignoreArch bool, names []string) error {
	// nothing to do if repo is initial
	if repo.IsNull() {
		return nil
	}

	// since the local repository will be changed, a lock must be set
	if err := repo.lock(); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot create lock")
		return errors.Wrapf(err, "cannot update repo '%s'", repo.Name)
	}
	defer func() { _ = repo.unlock() }()

	if len(names) == 0 {
		msg.PrintInfoln("updating all packages of repo '%s'", repo.Name)
	} else {
		msg.PrintInfoln("updating selected packages of repo '%s'", repo.Name)
	}

	if err := repo.download(); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot update repo '%s'", repo.Name)
		return errors.Wrapf(err, "cannot update repo '%s'", repo.Name)
	}

	// determine packages that need to be updates:
	// - get packages from repo DB
	_, infosDB, err := repo.infos(strToNames(names))
	if err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot update repo '%s", repo.Name)
		return errors.Wrapf(err, "cannot update repo '%s'", repo.Name)
	}
	// - determine which of them have updates
	updates := infosDB.updatesFromAUR()

	// nothing to to if there are no updates
	if len(updates) == 0 {
		msg.PrintInfoln("no updates available")
		return nil
	}

	// print information about available updates
	msg.PrintMsgln("updates available:")
	for name, upd := range updates {
		infoDB, exists := infosDB[name]
		if exists {
			fmt.Printf("    %s %s -> %s\n", name, infoDB.version, upd.version)
		}
	}

	// apply updates?
	if !noconfirm && !msg.UserOK("\n    continue") {
		return nil
	}

	// register the removal of temporary directories
	defer func() { _ = removeTmpDir() }()

	// clone PKGBUILD of to be updates packages
	pkgbuilds := updates.bases().pkgbuildsFromAUR(ignoreArch)

	// nothing to do if no PKGBUILD files could be downloaded
	if len(pkgbuilds) == 0 {
		msg.PrintInfoln("no PKGBUILD files cloned: exit")
		return nil
	}

	if err := repo.buildAdd(buildModeUpdate, false, false, nochroot, pkgbuilds); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("could not update packages in repo '%s'", repo.Name)
		if errUpload := repo.upload(); errUpload != nil {
			fmt.Println(errUpload)
			msg.PrintErrorln("cannot upload repo '%s'", repo.Name)
		}
		return errors.Wrapf(err, "cannot update repo '%s'", repo.Name)
	}

	if err := repo.upload(); err != nil {
		fmt.Println(err)
		msg.PrintErrorln("cannot update repo '%s'", repo.Name)
		return errors.Wrapf(err, "cannot update repo '%s'", repo.Name)
	}

	return nil
}
