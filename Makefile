PROG=crema

# completions
COMP=bash zsh fish
COMPDIR="./completions"

# use bash
SHELL=/usr/bin/bash

# set project VERSION to last tag name. If no tag exists, set it to v0.0.0
$(eval TAGS=$(shell git rev-list --tags))
ifdef TAGS
	VERSION=$(shell git describe --tags --abbrev=0)
else
	VERSION=v0.0.0	
endif

# setup the -ldflags option for go build
LDFLAGS=-ldflags "-X main.Version=$(VERSION)"

# build all executable, man page and completions
all: $(PROG).1 $(COMP)

.PHONY: all clean install lint release

# executable
$(PROG):
	go build -o ./$(PROG) -mod=mod $(LDFLAGS) ./src/cmd

# man page
$(PROG).1: ./doc/manpage.adoc
	@asciidoctor -b manpage -d manpage -o "$(PROG).8" ./doc/manpage.adoc

# completions
$(COMP): $(PROG)
	@mkdir -p $(COMPDIR)
	@./$(PROG) gencomp $@ > $(COMPDIR)/$@

lint:
	reuse lint
	golangci-lint run 

install:
	@install -Dm755 $(PROG) $(DESTDIR)/usr/bin/$(PROG)
	@install -Dm755 $(PROG)-all $(DESTDIR)/usr/bin/$(PROG)-all
	@install -Dm644 "$(PROG).8" -t "$(DESTDIR)/usr/share/man/man8/"
	@install -Dm644 $(COMPDIR)/bash $(DESTDIR)/usr/share/bash-completion/completions/$(PROG)
	@install -Dm644 $(COMPDIR)/zsh $(DESTDIR)/usr/share/zsh/site-functions/_$(PROG)
	@install -Dm644 $(COMPDIR)/fish $(DESTDIR)/usr/share/fish/vendor_completions.d/$(PROG).fish

# remove build results
clean:
	@rm -f "$(PROG)"
	@rm -f "$(PROG).8"
	@rm -rdf "$(COMPDIR)"

# (1) adjust version in PKGBUILD and in man documentation to RELEASE, commit
#     and push changes
# (2) create an annotated tag with name RELEASE
# syntax: make release RELEASE=vX.Y.Z
release:
	@if ! [ -z $(RELEASE) ]; then \
		REL=$(RELEASE); \
		sed -i -e "s/pkgver=.*/pkgver=$${REL#v}/" ./PKGBUILD; \
		sed -i -e "/Michael Picht/{n;s/^.*/Version $${REL#v}/}" ./doc/manpage.adoc; \
		git commit -a -s -m "release $(RELEASE)"; \
		git push; \
		git tag -a $(RELEASE) -m "release $(RELEASE)"; \
		git push origin $(RELEASE); \
	fi